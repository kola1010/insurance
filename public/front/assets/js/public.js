    (function($) {
      $(function() {
        $(".bx-caption > a[href='https://www.pzu.com.ua/useful/press/action011018.html'], .bx-caption > a[href='https://www.pzu.com.ua/ru/useful/press/action011018.html']").on('click', function() {
          ga('send', 'event', { eventCategory: 'AkciaKDD', eventAction: 'AkciaKDDclick'});
          console.log('AkciaKDD-AkciaKDDclick');
        });

        $("a.btn[href='https://www.pzu.com.ua/useful/press/action011018.html'], a.btn[href='https://www.pzu.com.ua/ru/useful/press/action011018.html']").on('click', function() {
          ga('send', 'event', { eventCategory: 'AkciaKDDMore', eventAction: 'AkciaKDDMoreClick'});
          console.log('AkciaKDDMore-AkciaKDDMoreClick');
        });

        $("a.btn[href='https://www.pzu.com.ua/individual/estate/kdd.html'], a.btn[href='https://www.pzu.com.ua/ru/individual/estate/kdd.html']").on('click', function() {
          ga('send', 'event', { eventCategory: 'AkciaStrMajno', eventAction: 'AkciaStrMajnoClick'});
          console.log('AkciaStrMajno-AkciaStrMajnoClick');
        });

        $("a.btn[href='https://www.pzu.com.ua/individual/travel/abroad/tourist.html'], a.btn[href='https://www.pzu.com.ua/ru/individual/travel/abroad/tourist.html']").on('click', function() {
          ga('send', 'event', { eventCategory: 'AkciaStrMajnoPodorozh', eventAction: 'AkciaStrMajnoPodorozhClick'});
          console.log('AkciaStrMajnoPodorozh-AkciaStrMajnoPodorozhClick');
        });

        $("a[href='/admin/upload/file/quick/Детальні умови_сайт.pdf']").on('click', function() {
          ga('send', 'event', { eventCategory: 'AkciaMajnoDownload', eventAction: 'AkciaMajnoDownloadClick'});
          console.log('AkciaMajnoDownload-AkciaMajnoDownloadClick');
        });

          $("#submit_corp_consult").on('click', function () {
              $(this).attr('disabled', 'disabled');
              ajax_query('default', 'sendconsult', form2array(document.forms['form_consult']));
          });
          $("#submit_corp_consult_modal").on('click', function () {
              $(this).attr('disabled', 'disabled');
              ajax_query('default', 'sendconsult2', form2array(document.forms['form_consult']))
          });
          $(".bx-caption a").on('click', function (e) {
              window.location = $(this).attr('href');
          })
      }); // end DOM ready
    })(jQuery); // end jQuery
function defUrl() {
  return '/';
}
function ShowLoad() {
  if (document.getElementById('pop_loading')) {
    document.getElementById('pop_loading').style.display = 'block';
  }
}
function HideLoad() {
  if (document.getElementById('pop_loading')) {
    document.getElementById('pop_loading').style.display = 'none';
  }
}
function ajax_query(c,a,data) {
  var query = new Array;
  if (a != 'getbanners') {
    ShowLoad();
  }
  query['c'] = c;
  query['a'] = a;

  query['d'] = data;

  if (a == 'sendreview') {
//    $("#myModal").modal('show');
    //alert('Сайт в розробці');
//    return;
  }

  name_ajax = defUrl()+'ajax.php';


  JsHttpRequest.query(name_ajax,query,function(result, errors) {

      debug_layer = document.getElementById('debug');
      if (debug_layer && errors) {
        debug_layer.innerHTML = "<pre>"+errors+"</pre>";
      }
//      ajax_callback(c,a,result);
      eval('ajax_cb_'+result['c']+'_'+result['a']+'(result["r"])');
      if (result['a'] != 'validquestion' && result['a'] != 'validmessage') {
          HideLoad();
      }
    },
    true
  );
}

function form2array(theForm) {
  var type;
  var arr = new Array;
  for(i=0; i<theForm.elements.length; i++){
    type = theForm.elements[i].type;
    if(type == "text" || type == "password" || type == "hidden" || type == "textarea" /*|| type == "button"*/ || type == "select-one"){
      arr[theForm.elements[i].name] = theForm.elements[i].value
    } else if(type == "checkbox" || type == "radio"){
      if ( theForm.elements[i].checked ) {
        arr[theForm.elements[i].name] = theForm.elements[i].value
      }
    } else if(type == "file") {
      arr[theForm.elements[i].name] = theForm.elements[i].files;
    }
  }
  return arr;
}

function ajax_callback(c,a,result) {
  alert('Recieved: ' + result);
}

function ajax_cb_user_md5(result) {
  alert('got md5: '+result);
}
function ajax_cb_default_appoint(result) {
  if (result['error'] == true) {
    alert(result['status']);
  } else {
    location.href = defUrl()+'appoint-ok.html';
  }
}
function ajax_cb_default_refresh(result) {
  document.getElementById('img_captcha').src = result;
}

function selectFilial(url, id) {
  if (id == 0) {
    location.href = url;
  } else {
    location.href = url + '?filial=' + id;
  }
}
function selectCity(url, id) {
  if (id == 0) {
    location.href = url;
  } else {
    location.href = url + '?city=' + id;
  }
}
function selectFilter(url) {
  ajax_query('default', 'filterfilials', {city: $('#select_city').val(), kind: $('#select_kind').val(), service: $('#select_service').val(), url: url});
//  var params = '';
//  if ($('#select_city').val() != '0') {
//    params = '?city=' + $('#select_city').val();
//  }
//  if ($('#select_kind').val() != '0') {
//    if (params == '') {
//      params += '?';
//    } else {
//      params += '&';
//    }
//    params += 'kind=' + $('#select_kind').val();
//  }
//  location.href = url + params;
}
function ajax_cb_default_filterfilials(result) {
  location.href = result;
}
function selectFilterSTO(url) {
  var params = '';
  if ($('#select_city').val() != '0') {
    params = '?city=' + $('#select_city').val();
  }
  if ($('#select_brand').val() != '0') {
    if (params == '') {
      params += '?';
    } else {
      params += '&';
    }
    params += 'brand=' + $('#select_brand').val();
  }
  if ($('#select_recomm').val() != '0') {
    if (params == '') {
      params += '?';
    } else {
      params += '&';
    }
    params += 'recomm';
  }
  location.href = url + params;
}
function ajax_cb_default_regionright(result) {
  $("#select_city").empty();
  $("#select_kind").empty();
  $("#select_kind").hide();
  $("#filials").empty();
  $("#filials").hide();
  if (result['show'] == true) {
    $("#select_city").show();
  } else {
    $("#select_city").hide();
  }
  if (result['cities'].length > 0) {
    opt = $('<option/>', {
      value: 0,
      html: result['word_select']
    })
    $("#select_city").append(opt);
    for (i=0; i<result['cities'].length; i++) {
      opt = $('<option/>', {
        value: result['cities'][i]['id'],
        html: result['cities'][i]['name']
      })
      $("#select_city").append(opt);
    }
  } else {

  }
}
function ajax_cb_default_cityright(result) {
  $("#select_kind").empty();
  $("#filials").empty();
  $("#filials").hide();
  // $("#select_address").empty();
  $("#select_address").prop('disabled', 'disabled');
  if (result['show'] == true) {
    $("#select_kind").show();
    $("#select_kind").prop('disabled', false);
  } else {
    $("#select_kind").hide();
    $("#select_kind").prop('disabled', 'disabled');
  }
  if (result['kinds'].length > 0) {
    opt = $('<option/>', {
      value: 0,
      html: result['word_select']
    })
    $("#select_kind").append(opt);
    for (i=0; i<result['kinds'].length; i++) {
      opt = $('<option/>', {
        value: result['kinds'][i]['id'],
        html: result['kinds'][i]['name']
      })
      $("#select_kind").append(opt);
    }
  } else {
  }
}
function ajax_cb_default_cityrightsz(result) {
  $("#select_kind").empty();
  $("#select_kind").hide();
  $("#filials").empty();
  $("#filials").hide();
  if (result['kinds'].length > 0) {
    ajax_query('default', 'kindright', {id:2, lang:result['lang'], city:$('#select_city').val(), emp: 1 });
//    if (result['show'] == true) {
//      $("#select_kind").show();
//    } else {
//      $("#select_kind").hide();
//    }
//    opt = $('<option/>', {
//      value: 0,
//      html: result['word_select']
//    })
//    $("#select_kind").append(opt);
//    for (i=0; i<result['kinds'].length; i++) {
//      opt = $('<option/>', {
//        value: result['kinds'][i]['id'],
//        html: result['kinds'][i]['name']
//      })
//      $("#select_kind").append(opt);
//    }
  } else {
    $("#filials").html('У вибраному місті агенцій не знайдено');
    $("#filials").show();
  }
}
function ajax_cb_default_kindright(result) {
  $("#filials").empty();
  if (result['show'] == true) {
    $("#filials").show();
  } else {
    $("#filials").hide();
  }
  if (result['filials'].length > 0) {
    for (i=0; i<result['filials'].length; i++) {
      div = $('<div/>', {
      });
      div.addClass('top-5');
      if (result['filials'][i]['phone']) {
        adr = $('<div/>', {
          html: result['filials'][i]['phone']
        });
        div.append(adr);
      }
      if (result['filials'][i]['email']) {
        adr = $('<div/>', {
          html: result['filials'][i]['email']
        });
        div.append(adr);
      }
      adr = $('<div/>', {
        html: result['filials'][i]['address']
      });
      div.append(adr);
      if (result['filials'][i]['url']) {
        adr = $('<a/>', {
          href: result['filials'][i]['url'],
          text: result['look_map']
        });
        div.addClass('color-3');
        div.append(adr);
      }

      $("#filials").append(div);
    }
  } else {
    if (result['emp'] == 1) {
      $("#filials").html('У вибраному місті агенцій не знайдено');
      $("#filials").show();
    }
  }
}
function ajax_cb_default_partnerright(result) {
  $("#partners").empty();
  if (result['show'] == true) {
    $("#partners").show();
  } else {
    $("#partners").hide();
  }
  div = $('<div/>', {
  });
  div.addClass('top-5');
  if (result['phone']) {
    adr = $('<div/>', {
      html: result['phone']
    });
    div.append(adr);
  }
  if (result['email']) {
    adr = $('<div/>', {
      html: result['email']
    });
    div.append(adr);
  }
  if (result['address']) {
    adr = $('<div/>', {
      html: result['address']
    });
    div.append(adr);
  }
  if (result['site']) {
    adr = $('<div/>', {
      html: result['site']
    });
    div.append(adr);
  }
  $("#partners").append(div);
}
function ajax_cb_default_selectcity(result) {
  $("#select_kind").empty();
  opt = $('<option/>', {
    value: 0,
    html: result['word_select']
  })
  $("#select_kind").append(opt);
  $("#select_address").empty();
  opt = $('<option/>', {
    value: 0,
    html: result['word_select2']
  })
  $("#select_address").append(opt);
  if (result['kinds'].length > 0) {
    for (i=0; i<result['kinds'].length; i++) {
      opt = $('<option/>', {
        value: result['kinds'][i]['id'],
        html: result['kinds'][i]['name']
      })
      $("#select_kind").append(opt);
    }
  }
}
function ajax_cb_default_selectkind(result) {
  $("#select_address").empty();
  $("#select_address").prop('disabled', 'disabled');
  opt = $('<option/>', {
    value: 0,
    html: result['word_select']
  })
  $("#select_address").append(opt);
  if (result['filials'].length > 0) {
    for (i=0; i<result['filials'].length; i++) {
      opt = $('<option/>', {
        value: result['filials'][i]['id'],
        html: result['filials'][i]['name']
      })
      $("#select_address").append(opt);
    }
    $("#select_address").prop('disabled', false);
  }
}
function ajax_cb_default_cityvac(result) {
  $("#select_vacancy").empty();
  opt = $('<option/>', {
    value: 0,
    html: result['word_select']
  })
  $("#select_vacancy").append(opt);
  if (result['vacs'].length > 0) {
    for (i=0; i<result['vacs'].length; i++) {
      opt = $('<option/>', {
        value: result['vacs'][i]['id'],
        html: result['vacs'][i]['name']
      })
      $("#select_vacancy").append(opt);
    }
  }
}
function ajax_cb_default_citytofilial(result) {
  $("#select_filialkasko").empty();
  opt = $('<option/>', {
    value: 0,
    html: result['word_select']
  })
  $("#select_filialkasko").append(opt);
  if (result['filials'].length > 0) {
    for (i=0; i<result['filials'].length; i++) {
      opt = $('<option/>', {
        value: result['filials'][i]['id'],
        html: result['filials'][i]['name']
      })
      $("#select_filialkasko").append(opt);
    }
    $("#select_filialkasko").removeAttr("disabled");
  } else {
    $("#select_filialkasko").attr('disabled', 'disabled');
  }
}
function ajax_cb_default_sendreview(result) {
  $("#myModalText").html(result['status']);
  $("#myModal").modal('show');
  if (result['error'] == true) {
//    $("#myModalText").html(result['status']);
//    $("#myModal").modal('show');
  } else {
    $("#name").val('');
    $("#opinion").val('');
    $("#email").val('');
    $("#phone").val('');
    $("#select_kind").empty();
    opt = $('<option/>', {
      value: 0,
      html: result['word_select']
    })
    $("#select_kind").append(opt);
    $("#select_address").empty();
    opt = $('<option/>', {
      value: 0,
      html: result['word_select2']
    })
    $("#select_address").append(opt);
  }
}
function sendQuestion() {
    ajax_query('default', 'validquestion', document.forms['form_question']);
}
function ajax_cb_default_validquestion(result) {
    var this_form = $('#form_question');
    this_form.find('.form-group').removeClass('is-invalid');
    if (result['error'] == true) {
        HideLoad();
        for (i=0; i<result['fields'].length; i++) {
            this_form.find('#group-'+result['fields'][i]).addClass('is-invalid');
            if (result['status'][ result['fields'][i] ]) {
                this_form.find('#group-'+result['fields'][i]+' > span.invalid-feedback').html(result['status'][ result['fields'][i] ]);
            }
        }
    } else {
        ShowLoad();
        var fc = $('#add_files').fileinput('getFilesCount');
        $('#file_count').val(fc);
        if (fc > 0) {
            $('#add_files').fileinput('upload');
            // setTimeout(function () {
            //     ajax_query('default', 'sendquestion', document.forms['form_question']);
            // }, 2000);
        } else {
            ajax_query('default', 'sendquestion', document.forms['form_question']);
        }
    }
}
function ajax_cb_default_sendquestion(result) {
    // console.log(result);
    var this_form = $('#form_question');
    this_form.find('.form-group').removeClass('is-invalid');
    if (result['error'] == true) {
        // $('#add_files').fileinput('refresh');
        for (i=0; i<result['fields'].length; i++) {
            this_form.find('#group-'+result['fields'][i]).addClass('is-invalid');
            if (result['status'][ result['fields'][i] ]) {
                this_form.find('#group-'+result['fields'][i]+' > span.invalid-feedback').html(result['status'][ result['fields'][i] ]);
            }
        }
    } else {
      if (result['debug'] == true) {
          console.log('DONE');
      } else {
          location.href = result['redirect'];
      }
    }
}
function ajax_cb_default_sendanketa(result) {
  $("#myModalText").html(result['status']);
  $("#myModal").modal('show');
  if (result['error'] == true) {
//    $("#myModalText").html(result['status']);
//    $("#myModal").modal('show');
  } else {
    $("#name").val('');
    $("#file").val('');
    $("#email").val('');
    $("#phone").val('+38 0');
    $("#select_city").val(0);
    $("#select_vacancy").empty();
    opt = $('<option/>', {
      value: 0,
      html: result['word_select2']
    })
    $("#select_vacancy").append(opt);
  }
}
function ajax_cb_default_sendorder(result) {
  $("#myModalText").html(result['status']);
  $("#myModal").modal('show');
  if (result['error'] == true) {
  } else {
    $("#name").val('');
    $("#address").val('');
//    $("#datebirth").val('');
    $("#city").val('');
    $("#zipcode").val('');
    $("#email").val('');
    $("#phone").val('+380');
    $("#seria").val('');
    $("#number").val('');
    $("#datedoc").val('');
    $("#model").val('');
    $("#shassi").val('');
    $("#gosnomer").val('');
    $("#reg_place").val('');
    $("#fraud").attr('checked', false);
    $("#taxi").attr('checked', false);
    $("#pensioner").attr('checked', false);
    $("#select_region").val(0);
  }
}
function ajax_cb_default_sendkasko(result) {
  $("#myModalText").html(result['status']);
  $("#myModal").modal('show');
  if (result['error'] == true) {
  } else {
    $("#name").val('');
    $("#phone").val('');
    $("#amount").val('');
    $("#select_citykasko").val(0);
    $("#select_filialkasko").empty();
  }
}
function ajax_cb_default_drawing(result) {
  $("#myModalText").html(result['status']);
  $("#myModal").modal('show');
  if (result['error'] == true) {
  } else {
    $("#name").val('');
    $("#birth").val('');
    $("#contract").val('');
    $("#mobile").val('');
    $("#mobile2").val('');
    $("#email").val('');
  }
}
function ajax_cb_default_sendconsult(result) {
  // console.log(result);
  // return;
  HideLoad();
  $("#myModalText").html(result['status']);
  $("#myModal").modal('show');
  if (result['error'] == true) {
      $("#submit_corp_consult").attr('disabled', false);
    // document.getElementById('img_captcha').src = '/securimage3/securimage_show.php?' + Math.random();
  } else {
    $("#name").val('');
    $("#company").val('');
    $("#phone").val('');
    $("#email").val('');
    $("#city").val(0);
    $("#filial").empty();
    $("#submit_corp_consult").attr('disabled', false);
    if (result['ga'] != '') {
      eval(result['ga']);
    }
    if (result['dataLayer']) {
        dataLayer.push({
            'event': 'gaTriggerEvent',
            'gaEventCategory': result['dataLayer']['cat'],
            'gaEventAction': result['dataLayer']['act'],
            'gaEventLabel': result['dataLayer']['label']
        });
        // console.log(result['dataLayer']['act']);
    }
    // ga('send', 'event', { eventCategory: 'ConsultationUR', eventAction: 'Send'});
  }
}
function ajax_cb_default_sendconsult2(result) {
  HideLoad();
  $("#myModalText").html(result['status']);
  $("#myModal").modal('show');
  if (result['error'] == true) {
      $("#submit_corp_consult_modal").attr('disabled', false);
  } else {
    $("#name").val('');
    $("#phone").val('');
    $("#company").val('');
    $("#email").val('');
    $("#comment").val('');
    $('#modal_form_legalconsult_2').modal('hide');
      $("#submit_corp_consult_modal").attr('disabled', false);
    if (result['ga'] != '') {
      eval(result['ga']);
    }
      if (result['dataLayer']) {
          dataLayer.push({
              'event': 'gaTriggerEvent',
              'gaEventCategory': result['dataLayer']['cat'],
              'gaEventAction': result['dataLayer']['act'],
              'gaEventLabel': result['dataLayer']['label']
          });
          // console.log(result['dataLayer']['act']);
      }
    // ga('send', 'event', { eventCategory: 'ConsultationUR', eventAction: 'Send'});
  }
}
function ajax_cb_default_toorder(result) {
  location.href = result['url'];
}
function selectCategory(url, id) {
  $('#' + id).toggleClass('button_active');
  $('#' + id).toggleClass('button_cat');
  active = $('.button_active');
  r = '';
  for (i=0; i<active.length; i++) {
    r += active[i].id + ',';
  }
  r = r.substring(0,r.length-1);
  if (r == '') {
    location.href = url;
  } else {
    location.href = url + '?params=' + r;
  }
  //ajax_query('default', 'setcat', id);
}
function ajax_cb_default_search(result) {
  location.href = result;
}

function goToPay() {
  ajax_query('default', 'gotoonline', form2array(document.forms['form_pay_online']))
}
function ajax_cb_default_gotoonline(result) {
  if (result['error'] == true) {
    $("#myModalText").html(result['status']);
    $("#myModal").modal('show');
  } else {
    if (result['paytype'] == 'easypay') {
      location.href = result['url'];
    } else if (result['paytype'] == 'oschad') {
//      alert(result['mac_row'].length);
      var form = $("#form_oschad");
      $("#CURRENCY").val(result['CURRENCY']);
      $("#ORDER").val(result['ORDER']);
      $("#DESC").val(result['DESC']);
      $("#MERCH_NAME").val(result['MERCH_NAME']);
      $("#MERCH_URL").val(result['MERCH_URL']);
      $("#MERCHANT").val(result['MERCHANT']);
      $("#TERMINAL").val(result['TERMINAL']);
      $("#EMAIL").val(result['EMAIL']);
      $("#TRTYPE").val(result['TRTYPE']);
      $("#COUNTRY").val(result['COUNTRY']);
      $("#MERCH_GMT").val(result['MERCH_GMT']);
      $("#TIMESTAMP").val(result['TIMESTAMP']);
      $("#NONCE").val(result['NONCE']);
      $("#BACKREF").val(result['BACKREF']);
      $("#P_SIGN").val(result['P_SIGN']);
      $("#amount").val(result['amount']);
      $("#Amount").val(result['amount']);
      $("#AMOUNT").val(result['amount']);

      form.submit();
    } else if (result['paytype'] == 'aval') {
      var form = $("#form_aval");
      $("#MerchantID").val(result['MerchantID']);
      $("#TerminalID").val(result['TerminalID']);
      $("#TotalAmount").val(result['amount']);
      $("#Currency").val(result['Currency']);
      $("#SD").val(result['SD']);
      $("#OrderID").val(result['OrderID']);
      $("#PurchaseTime").val(result['PurchaseTime']);
      $("#PurchaseDesc").val(result['PurchaseDesc']);
      $("#Signature").val(result['sign']);

      form.submit();
    }
//    $("#name").val('');
//    $("#company").val('');
//    $("#phone").val('');
//    $("#email").val('');
//    $("#city").val(0);
  }
}

function closeCube() {
  ajax_query('default', 'closecube', 1);
}
function ajax_cb_default_closecube(result) {
  $("#flashContent").hide();
}
// ***** CABINET **********
function setModalCloseLink(link) {
  $("#btn-modal-close-reload").attr("href", defUrl() + link);
  $("#btn-modal-close-reload").show();
  $("#btn-modal-close").hide();
}
function clickForgot() {
//  if ($("#forgot").checked) {
  if ($("#forgot").prop('checked')) {
    $("#pass").attr("disabled","disabled");
      $("#group-newpass").hide();
      $("#group-newpass2").hide();
      $("#opennewpass").val('0');
  } else {
    $("#pass").attr("disabled",false);
  }
}
function ajax_cb_default_reg(result) {
  HideLoad();
  if (result['error'] == true) {
    $("#myModalText").html(result['status']);
    $("#myModal").modal('show');
  } else {
    $("#email").val('');
    $("#name").val('');
    $("#number").val('');
    $("#myModalText").html(result['status']);
    $("#myModal").modal('show');
    if (result['reload_after'] == true) {
      setModalCloseLink(result['reload_link']);
    }
  }
}
function ajax_cb_default_auth(result) {
  HideLoad();
  if (result['error'] == true) {
    $("#myModalText").html(result['status']);
    $("#myModal").modal('show');
  } else {
    if (result['opennewpass'] == true) {
      $("#group-newpass").show();
      $("#group-newpass2").show();
      $("#opennewpass").val('1');
      $("#myModalText").html(result['status']);
      $("#myModal").modal('show');
    } else if (result['noreload'] == 1) {
      $("#auth_email").val('');
      $("#pass").val('');
      $("#newpass").val('');
      $("#newpass2").val('');
      $("#myModalText").html(result['status']);
      $("#myModal").modal('show');
    } else {
      window.location.href = defUrl() + result['redirect'];
    }
  }
}
function ajax_cb_default_forgot(result) {
  HideLoad();
  if (result['error'] == true) {
      $("#myModalText").html(result['status']);
      $("#myModal").modal('show');
  } else {
      $("#auth_email").val('');
      $("#myModalText").html(result['status']);
      $("#myModal").modal('show');
      if (result['reload_after'] == true) {
        setModalCloseLink(result['reload_link']);
      }
  }
}
function logout() {
  ajax_query('default','logout',1);
}
function ajax_cb_default_logout() {
  window.location.href = defUrl()+"pclife/registration.html";
}
// cabinet send message
    function sendMessage() {
        ShowLoad();
        var total_size = $('#add_files').fileinput('getTotalSize');
        if ( (total_size / 1024 / 1024) > 10) {
            HideLoad();
            var this_form = $('#form');
            this_form.find('#group-add_files').addClass('is-invalid');
            this_form.find('#group-add_files > span.invalid-feedback').html('Загальний розмір файлів перевищує 10МБ');
            return;
        }
        ajax_query('default', 'validmessage', document.forms['form_login']);
    }
    function ajax_cb_default_validmessage(result) {
        var this_form = $('#form');
        this_form.find('.form-group').removeClass('is-invalid');
        if (result['error'] == true) {
            HideLoad();
            for (i=0; i<result['fields'].length; i++) {
                this_form.find('#group-'+result['fields'][i]).addClass('is-invalid');
                if (result['status'][ result['fields'][i] ]) {
                    this_form.find('#group-'+result['fields'][i]+' > span.invalid-feedback').html(result['status'][ result['fields'][i] ]);
                }
            }
        } else {
            var fc = $('#add_files').fileinput('getFilesCount');
            $('#file_count').val(fc);
            if (fc > 0) {
                $('#add_files').fileinput('upload');
            } else {
                ajax_query('default', 'sendmessage', document.forms['form_login']);
            }
        }
    }
    function ajax_cb_default_sendmessage(result) {
        var this_form = $('#form');
        this_form.find('.form-group').removeClass('is-invalid');
        HideLoad();
        if (result['error'] == true) {
            // $('#add_files').fileinput('refresh');
            for (i=0; i<result['fields'].length; i++) {
                this_form.find('#group-'+result['fields'][i]).addClass('is-invalid');
                if (result['status'][ result['fields'][i] ]) {
                    this_form.find('#group-'+result['fields'][i]+' > span.invalid-feedback').html(result['status'][ result['fields'][i] ]);
                }
            }
        } else {
            if (result['debug'] == true) {
                console.log('DONE');
            } else {
                $("#theme").val('');
                $("#message").val('');
                $("#message").html('');
                $("#myModal").find("#myModalText").html(result['status']);
                $("#add_files").fileinput('reset');
                $('.upload-files').remove();
                $("#myModal").modal('show');
                // location.href = result['redirect'];
            }
        }
    }
// function ajax_cb_default_sendmessage(result) {
//   HideLoad();
//   if (result['error'] == true) {
//       $("#myModal").find("#myModalText").html(result['status']);
//       $("#myModal").modal('show');
//   } else {
//       $("#theme").val('');
//       $("#file").val('');
//       $("#message").val('');
//       $("#message").html('');
//       $("#myModal").find("#myModalText").html(result['status']);
//       $("#myModal").modal('show');
//   }
// }
function ajax_cb_default_sendcareer(result) {
  HideLoad();
  if (result['error'] == true) {
    $("#myModalText").html(result['status']);
    $("#myModal").modal('show');
  } else {
    $("#name").val('');
    $("#phone").val('');
    $("#message").val('');
    $("#message").html('');
    $("#myModalText").html(result['status']);
    $("#myModal").modal('show');
  }
}
function ajax_cb_default_sendkaskocons(result) {
    // console.log(result);
    HideLoad();
    $("#block_name").removeClass('error');
    $("#block_phone").removeClass('error');
    $("#block_city").removeClass('error');
    $("#block_email").removeClass('error');
    if (result['error'] == true) {
        for (i=0; i<result['fields'].length; i++) {
          $("#block_"+result['fields'][i]).addClass('error');
        }
        $('#submit_phone_consult').attr('disabled', false);
        // if (result['mainstatus'] != '') {
        //     $("#myModal").find("#myModalText").html(result['mainstatus']);
        //     $("#myModal").modal('show');
        // }
    } else {
        $("form[name='form_kasko_phone'] #name").val('');
        $("form[name='form_kasko_phone'] #phone").val('');
        $("form[name='form_kasko_phone'] #city").val('');
        $("form[name='form_kasko_phone'] #filial").val('');
        $("form[name='form_kasko_phone'] #email").val('');
        $("#add_fields").val('0');
        $("#other_fields").hide();
        $("#show_fields").html("Показати додаткові поля");
        $("#myModalText").html(result['status']);
        // $("#myModal").modal('show');
        // просто так не открывается модалка из-за долбанного калькулятора осаго
        $('#btn-modal').click();
        $('#submit_phone_consult').attr('disabled', false);
        if (result['ga'] != '') {
              eval(result['ga']);
        }
        if (result['dataLayer']) {
            dataLayer.push({
                'event': 'gaTriggerEvent',
                'gaEventCategory': result['dataLayer']['cat'],
                'gaEventAction': result['dataLayer']['act'],
                'gaEventLabel': result['dataLayer']['label']
            });
            // console.log(result['dataLayer']['act']);
        }
    }
}

function showCorp(value) {
  ajax_query('default','showcorp',{'id':value, 'lang':$("#lang").val()});
}
function ajax_cb_default_showcorp(result) {
  ga('send', 'event', { eventCategory: 'ChooseBranch', eventAction: 'ChooseBranchClick'});
  $("#show_phone").html(result);
}

function fillSelect(select_item, list_values) {
  select_item.empty();
  $.each(list_values, function(key, value) {
    opt = $("<option/>", {
      value: value['id'],
      html: value['name']
    });
    select_item.append(opt);
  });
}

function ajax_cb_default_geoip(result) {
  // console.log = result;
  // alert(result);
  location.href = result;
}

function openPrintBill() {
  ajax_query('default','openprintbill',form2array(document.forms['form_bill']));
}
function ajax_cb_default_openprintbill(result) {
  window.open(defUrl()+"print.html",'_blank');
  // location.href = defUrl()+"print.html";
}
/***** CALC CASKO JS ******/
$(document).ready(function() {
   $("#submit_modal_consultation").on('click', function() {
     $(this).attr('disabled', 'disabled');
     submitCalcKasko();
   });
   $('#submit_phone_consult').on('click', function() {
       $(this).attr('disabled', 'disabled');
       ajax_query('default','sendkaskocons',form2array(document.forms['form_kasko_phone']))
   });
});
function selectCatVehicle(id) {
$( "#group-catvehicle img" ).each(function() {
  $( this ).attr("src",$("#img"+$(this).attr('lang')).val());
  $("#catvehicle"+$(this).attr('lang')).attr("checked", false);
});
//  for (i=1; i<=5; i++) {
//    $("#group-catvehicle img[lang='"+i+"']").attr("src",$("#img"+id).val());
//    $("#catvehicle"+i).attr("checked", false);
//  }
  $("#group-catvehicle img[lang='"+id+"']").attr("src",$("#selimg"+id).val());
  $("#catvehicle"+id).attr("checked", "checked");
  $("#catvehicle"+id).click();
  if (id == 3) {
    $("#marka_s").show();
    $("#marka").hide();
//    $("#model_s").show();
//    $("#model_s").attr('disabled','disabled');
//    $("#model").hide();
  } else {
    $("#marka_s").hide();
    $("#marka").show();
//    $("#model_s").hide();
//    $("#model_s").attr('disabled','disabled');
//    $("#model").show();
  }
}
function openModalConsultForm(preform) {
  openModalKasko(preform);
}
function openModalKasko(preform) {
  if (preform != 0) {

    ajax_query('default', 'openkasko', document.forms[preform]);
  } else {
    $("#form_kasko #catvehicle").val(0);
    $("#form_kasko #marka").val('');
    $("#form_kasko #model").val('');
    $("#form_kasko #prodyear").val(0);
    $("#form_kasko #mileage").val(0);
    $("#form_kasko #driverage").val(0);
    $("#form_kasko #driverexper").val(0);
    $("#avto_info").hide();
    showModalKasko();
  }
}
function showModalKasko() {
  // $("#modal_form_kasko").modal('show');
// просто так не открывается модалка из-за долбанного калькулятора осаго
    $('#btn-modal-consult').click();  
}
function ajax_cb_default_openkasko(result) {

  if (result['error'] == true) {
    $("#myModalText").html(result['status']);
    $("#myModal").modal('show');
  } else {
    $("#form_kasko").find("#catvehicle").val(result['catvehicle']);
    $("#form_kasko #marka").val(result['marka_text']);
    $("#form_kasko #model").val(result['model']);
    $("#form_kasko #prodyear").val(result['prodyear_text']);
    $("#form_kasko #mileage").val(result['mileage']);
    $("#form_kasko #driverage").val(result['driverage_text']);
    $("#form_kasko #driverexper").val(result['driverexper']);

    $("#form_kasko #catvehicle_text").html(result['catvehicle_text']);
    $("#form_kasko #marka_text").html(result['marka_text']);
    $("#form_kasko #prodyear_text").html(result['prodyear_text']);
    $("#form_kasko #driverage_text").html(result['driverage_text']);
    $("#avto_info").show();
    showModalKasko();
  }
}
function getFilialsByCity(value, form_name, this_is_life) {
  ajax_query('default', 'getfilials', {'lang':$("#lang").val(), 'city':value, 'form_name':form_name, 'this_is_life':this_is_life});
}
function ajax_cb_default_getfilials(result) {
  var filials = $("#"+result['form_name']).find("#filial");
  filials.empty();
  var count_filials = result['list'].length;
  $.each(result['list'], function(key, value) {
    opt = $("<option/>", {
      value: value['id'],
      html: value['name']
    });
    if (value['id'] == 'onlineshop' || value['id'] == 179 || value['id'] == 9999 || count_filials == 1) {
      opt.prop('selected','selected');
    }
    filials.append(opt);
  });
    if (filials.hasClass('chosen-select-no-single')) {
        filials.find('option:first').html('');
        filials.prop('disabled', false);
        filials.trigger("chosen:updated");
    }
  if (result['list_size'] == 1) {
    $("#"+result['form_name']).find("#filial > option:last").prop("selected","selected");
  }
  if (result['this_is_life'] == 1) {
    if (result['list_size'] <= 1) {
      $("#"+result['form_name']).find("#block_filial").hide();
      $("#"+result['form_name']).find("#block_nf_filial").hide();
      $("#"+result['form_name']).find("#filial_require").val(0);
    } else {
      $("#"+result['form_name']).find("#block_filial").show();
      $("#"+result['form_name']).find("#block_nf_filial").show();
      $("#"+result['form_name']).find("#filial_require").val(1);
    }
  }
}
function ajax_cb_default_selectlife(result) {
  // console.log(result);
    var ins = $('#insurance');
    ins.empty();
    $.each(result['list'], function(key, value) {
        opt = $("<option/>", {
            value: key,
            html: value[result['lang']]
        });
        ins.append(opt);
    });
    ins.trigger("chosen:updated");
}

function submitCalcKasko() {
  ajax_query('default', 'kasko2015', document.forms['form_kasko']);
}
function ajax_cb_default_kasko2015(result) {
    // debug
  // console.log(result);
  // result;
  HideLoad();
  $("#block_nf_name").removeClass('error');
  $("#block_nf_phone").removeClass('error');
  $("#block_nf_city").removeClass('error');
  $("#block_nf_filial").removeClass('error');
  $("#block_nf_email").removeClass('error');
  $("#error_choose_prod").hide();
  if (result['error'] == true) {
      for (i=0; i<result['fields'].length; i++) {
        if (result['fields'][i] == 'choose_prod') {
          $("#error_choose_prod").show();
        } else {
          $("#block_nf_"+result['fields'][i]).addClass('error');
        }
      }
      $('#submit_modal_consultation').attr('disabled', false);
//    $("#myModalText").html(result['status']);
//    $("#myModal").modal('show');
  } else {
      // $("#modal_form_kasko").modal('hide');
      $("#modal_form_kasko").find("#btn-close-modal").click();
      $("#form_kasko #name").val('');
      $("#form_kasko #phone").val('');
      $("#form_kasko #city").val('');
      $("#form_kasko #email").val('');
      $("#add_fields").val('0');
      $("#other_fields").hide();
      $("#show_fields").html("Показати додаткові поля");
      $("#myModalText").html(result['status']);
      // debug
      // $("#myModalText").html(result);
      // $("#myModal").modal('show');
      $('#btn-modal').click();
      $('#submit_modal_consultation').attr('disabled', false);
      if (result['ga'] != '') {
        eval(result['ga']);
      }
      if (result['dataLayer']) {
          dataLayer.push({
              'event': 'gaTriggerEvent',
              'gaEventCategory': result['dataLayer']['cat'],
              'gaEventAction': result['dataLayer']['act'],
              'gaEventLabel': result['dataLayer']['label']
          });
          // console.log(result['dataLayer']['act']);
      }
  }
}
function getModels(value) {
  ajax_query('default','getmodels',document.forms['preform_kasko']);
}
function ajax_cb_default_getmodels(result) {
  var select = $("#model_s");
  select.show();
  select.attr('disabled',false);
  fillSelect(select, result['list']);
  $("#model").hide();
}
function changeModel(value) {
  ajax_query('default','changemodel',{'lang':$("#lang").val(), 'model':value});
}
function ajax_cb_default_changemodel(result) {
  if (result == false) {
    $("#model").hide();
  } else {
    $("#model").show();
  }
}


function GetBanner(ids) {
    var data = {'ids': ids};
    data['lang'] = $("#lang").val();
    ajax_query('default', 'getbanners', data);
}
function ajax_cb_default_getbanners(result) {
  $(".main-slider").append(result['html']);
  $('#js-main-slider').bxSlider({
    auto: true,
    pager: true,
    controls: false,
    moveSlides: 1,
    autoHover: true,
    touchEnabled: false
  });  
}
// TABS
$(document).ready(function() {
  var isMobile = /Android|webOS|iPhone|iPad|iPod|BlackBerry|Windows Phone/i.test(navigator.userAgent), $flag;
  // tabs
  $(".nav-tabs > li").on('click', function() {
    var val = $(this).width() + $(this).offset().left - $(".nav-tabs").width() - 16;
    var val2 = $(this).offset().left - 14;
    if (val > 0 || val2 < 0) {
      $(".nav-tabs").animate({ scrollLeft: $(".nav-tabs > li.active").offset().left - 30 }, 1000);
    }
  });
  $(".nav-tabs").on('scroll', function() {
      if (($(".nav-tabs > li:first-child").offset().left - 14) < 0) {
          $(this).parent().addClass('scroll-left');
      } else {
          $(this).parent().removeClass('scroll-left');
      }
      var last = $(".nav-tabs > li:last-child");
      var val = last.width() + last.offset().left - $(".nav-tabs").width() - 16;
      if (val < 0) {
          $(this).parent().addClass('scroll-right');
      } else {
          $(this).parent().removeClass('scroll-right');
      }
  });
  // lazy load img
  $("img").each(function() {
      if ($(this).attr("src") == 'undefined' || $(this).attr("src") == '' || !$(this).attr("src")) {
          $(this).attr("src", $(this).attr("data-src"));
      }
  })
  // youtube
  $(".youtube_block").click(function() {
      var iframe_url = "https://www.youtube.com/embed/" + this.id + "?autoplay=1&autohide=1";
       if ($(this).data('params')) iframe_url+='&'+$(this).data('params');
      var iframe = $('<iframe/>', {'frameborder': '0', 'src': iframe_url, 'width': '100%' , 'height': $(this).children('.video__img').height() })
      $(this).replaceWith(iframe);
  });  

  if (isMobile) { 
    $(function() { 
      var box = $('.responsive-tabs-container'); // float-fixed block
      if (box.length > 0) {
        var top = box.offset().top - parseFloat(box.css('marginTop').replace(/auto/, 0));
        $(window).scroll(function(){
            var windowpos = $(window).scrollTop();
            if(windowpos < top) {
                box.removeClass('fix-tabs');
            } else {
                box.addClass('fix-tabs');
            }
        });
      }  
    });
  }

// CUSTOM_SELECT
    var config = {
        '.chosen-select'           : {},
        '.chosen-select-deselect'  : { allow_single_deselect: true },
        '.chosen-select-no-single' : { disable_search_threshold: 10 },
        '.chosen-select-no-results': { no_results_text: 'Oops, nothing found!' },
        '.chosen-select-rtl'       : { rtl: true },
        '.chosen-select-width'     : { width: '95%' }
    }
    for (var selector in config) {
        if ($(selector).length > 0) {
            $(selector).chosen(config[selector]);
        }
    }

});