@extends('front-layouts.master')

@section('title', 'Контакти')

@section('breadcrumb')
    @include('front-layouts.helpers.breadcrumb', [
        'links' => [
            ['url' => route('front.homepage'), 'name' => 'Главная'],
            ['url' => null, 'name' => 'Контакти']
        ]
    ])
@stop

@section('content')
    <!-- BEGIN: content-page -->
    <div class="content-page">

        <!--<span></span>-->
        <h1>Контакти</h1>




        <!-- BEGIN: content-page__banner -->
        <div class="content-page__banner">
            <img src="/front/assets/content/section/113/113_pict3.jpg" alt="">
        </div>
        <!-- END: content-page__banner -->



        <!-- 2 column layout -->
        <div class="row">
            <div class="col-sm-9">

                <!-- BEGIN: text-block -->
                <div class="text-block">
                    <p>&nbsp;</p>

                    <table border="0" cellpadding="0" cellspacing="0" style="width: 100%;">
                        <tbody>
                        <tr>
                            <td>
                                <h3>Центральний офіс</h3>

                                <div itemscope="" itemtype="https://schema.org/PostalAddress"><meta itemprop="name" content="Страхова компанія ПЗУ Україна"><meta itemprop="addressCountry" content="UA">
                                    <p><span itemprop="postalCode">04112</span>, <span itemprop="addressLocality">м. Київ</span>,<br />
                                        <span itemprop="streetAddress">вул. Дегтярівська, 62</span>,<br />
                                        тел: <a content="+380442386238" href="tel:+380442386238" itemprop="telephone">+38 (044) 238 62 38</a>, <a content="+380445810400" href="tel:+380445810400" itemprop="telephone">+38(044) 581 04 00</a><br />
                                        факс: <span content="+380445810455" itemprop="faxNumber">(044) 581 04 55</span></p>
                                </div>

                                <p>email: <a href="mailto:for-pzu@pzu.com.ua">for-pzu@pzu.com.ua</a></p>
                            </td>
                            <td>
                                <h4><strong>Дивіться також:</strong></h4>

                                <ul>
                                    <li><a href="useful/sto.html">Мапа регіональних офісів</a></li>
                                    <li><a href="https://shop.pzu.com.ua">Інтернет-магазин</a></li>
                                </ul>
                            </td>
                        </tr>
                        </tbody>
                    </table>

                    <p><iframe frameborder="0" height="500" src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d2539.8450517558877!2d30.43745300000001!3d50.462610000000005!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x40d4cc35bfc1ca77%3A0xd3f52d2bb4ee9f8c!2z0LLRg9C7LiDQlNC10LPRgtGP0YDRltCy0YHRjNC60LAsIDYyLCDQmtC40ZfQsiwg0LzRltGB0YLQviDQmtC40ZfQsg!5e0!3m2!1sru!2sua!4v1416393424259" style="border:0" width="700"></iframe><!--<iframe frameborder="0" height="500" marginheight="0" marginwidth="0" scrolling="no" src="/front/assets/https://maps.google.com.ua/maps?f=q&source=s_q&hl=ru&geocode=&q=%D0%B2%D1%83%D0%BB.+%D0%94%D0%B5%D0%B3%D1%82%D1%8F%D1%80%D1%96%D0%B2%D1%81%D1%8C%D0%BA%D0%B0,+62&sll=50.402036,30.532691&sspn=0.722138,1.783905&t=m&ie=UTF8&hq=&hnear=%D0%94%D0%B5%D0%B3%D1%82%D1%8F%D1%80%D1%96%D0%B2%D1%81%D1%8C%D0%BA%D0%B0+%D0%B2%D1%83%D0%BB.,+62,+%D0%9A%D0%B8%D0%B5%D0%B2,+%D0%B3%D0%BE%D1%80%D0%BE%D0%B4+%D0%9A%D0%B8%D0%B5%D0%B2&z=14&ll=50.462643,30.437668&output=embed" width="700"></iframe><br />
	<small><a href="https://maps.google.com.ua/maps?f=q&source=embed&hl=ru&geocode=&q=%D0%B2%D1%83%D0%BB.+%D0%94%D0%B5%D0%B3%D1%82%D1%8F%D1%80%D1%96%D0%B2%D1%81%D1%8C%D0%BA%D0%B0,+62&sll=50.402036,30.532691&sspn=0.722138,1.783905&t=m&ie=UTF8&hq=&hnear=%D0%94%D0%B5%D0%B3%D1%82%D1%8F%D1%80%D1%96%D0%B2%D1%81%D1%8C%D0%BA%D0%B0+%D0%B2%D1%83%D0%BB.,+62,+%D0%9A%D0%B8%D0%B5%D0%B2,+%D0%B3%D0%BE%D1%80%D0%BE%D0%B4+%D0%9A%D0%B8%D0%B5%D0%B2&z=14&ll=50.462643,30.437668" style="color:#0000FF;text-align:left">Дивитися збільшену мапу</a></small>--></p>

                    <h3>Юридичні адреси:</h3>

                    <table border="1" cellpadding="1" cellspacing="1" style="width: 100%;">
                        <tbody>
                        <tr>
                            <td>
                                <p><strong>ПРИВАТНЕ АКЦІОНЕРНЕ ТОВАРИСТВО&nbsp;&quot;СТРАХОВА&nbsp; &nbsp; КОМПАНІЯ&nbsp;&quot;ПЗУ УКРАЇНА&quot;</strong></p>

                                <p>04053, м. Київ, Шевченківський район, вул. Січових&nbsp;Стрільців, 40 (код території за КОАТУУ &ndash; 8039100000).<br />
                                    Код фінансової установи за ЄДРПОУ &ndash; 20782312</p>

                                <p><u>Розклад роботи:</u></p>

                                <p>пн.-пт.: 09:00-18:00</p>

                                <p>сб-нд. - вихідні</p>
                            </td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>
                                <p><strong>ПРИВАТНЕ АКЦІОНЕРНЕ ТОВАРИСТВО &quot;СТРАХОВА КОМПАНІЯ &quot;ПЗУ УКРАЇНА СТРАХУВАННЯ ЖИТТЯ&quot;&nbsp;</strong></p>

                                <p>04053, м. Київ, Шевченківський район, вул. Січових Стрільців,&nbsp; 42 (код території за КОАТУУ &ndash; 8039100000).<br />
                                    Код фінансової установи за ЄДРПОУ &ndash; 32456224</p>

                                <p><u>Розклад роботи:</u></p>

                                <p>пн.-пт.: 09:00-18:00</p>

                                <p>сб-нд. - вихідні</p>
                            </td>
                        </tr>
                        </tbody>
                    </table>

                    <p>&nbsp;</p>

                </div>
                <!-- END: text-block -->

            </div>
            <!-- END: col -->

            <div class="col-sm-3">
                <aside class="sidebar">
                    <!-- BEGIN: sidebar__block -->
                    <!-- END: sidebar__block -->
                    <!-- BEGIN: sidebar__block -->

                    <div class='bottom-2'><a href='useful/sto.html'><img src='/front/assets/content/rightblock/90/90_pict.png'></a></div>

                    <!-- END: sidebar__block -->
                </aside>
            </div>
            <!-- END: col -->

        </div>
        <!-- END: row -->

    </div>
    <!-- END: content-page -->
@stop