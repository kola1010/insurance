<form action="{{ route('front.feedbackSubmit') }}" method="post" name="form_kasko_phone" id="form_kasko_phone">
    <input type="hidden" name="type" value="{{ $typeFeedback }}">
    @csrf
    <div class="box box-new">
        <div class="head mb15">
            Замовити телефонну консультацію що до <span>страхування майна</span>
        </div>
        <div id="block_name">
            <div class="mb5">
                <div>Прізвище, Ім'я</div>
            </div>
            <div class="mb15">
                <input class="focus_noerror" required type="text" value="" name="name" id="name">
            </div>
        </div>
        <div id="block_phone">
            <div class="mb5">
                <div>Телефон</div>
            </div>
            <div class="mb15">
                <input class="focus_noerror" required type="text" value="" name="phone" id="phone">
            </div>
        </div>
        <div class="mb15">
            <label for="agree" style="text-align:left;font-size: 13px;line-height: 14px;">
                <input type="checkbox" value="1" name="agree" id="agree" checked="checked">
                Я даю згоду на обробку моїх персональних даних, наданих у даній формі – заявці на консультацію.
            </label>
        </div>
        <div class="">
            <button class="btn" style="line-height:14px;padding: 5px 18px; white-space: normal">Замовити консультацію</button>
            <button class="btn hide" id="btn-modal" data-toggle="modal" data-target="#myModal">for modal</button>
        </div>
    </div>
</form>