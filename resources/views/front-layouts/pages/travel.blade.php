@extends('front-layouts.master')

@section('title', 'Туристичне страхування')

@section('breadcrumb')
    @include('front-layouts.helpers.breadcrumb', [
        'links' => [
            ['url' => route('front.homepage'), 'name' => 'Главная'],
            ['url' => null, 'name' => 'Туристичне страхування']
        ]
    ])
@stop

@section('content')
    <!-- BEGIN: content-page -->
    <div class="content-page">

        <!--<span></span>-->
        <h1>Туристичне страхування</h1>




        <!-- BEGIN: content-page__banner -->
        <div class="content-page__banner">
            <img src="/front/assets/content/section/12/12_pict3.jpg" alt="">
        </div>
        <!-- END: content-page__banner -->



        <!-- 2 column layout -->
        <div class="row">
            <div class="col-sm-9">

                <!-- BEGIN: car-insurance -->
                <div class="car-insurance">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="car-insurance__head-text">
                                <p>Наближається довгоочікувана відпустка чи відрядження. На жаль, під час будь-якої подорожі можуть трапитися будь-які ситуації: приємні і не дуже. Маючи на руках страховий поліс, Ви в будь-який момент (24 години на добу) можете отримати допомогу.&nbsp;Щоб мати дійсно&nbsp;надійний захист, звертайтеся PZU, робота якої перевірена часом і сотнями тисяч туристів.</p>
                            </div>
                            @include('front-layouts.pages.helpers.form-feedback', ['typeFeedback' => 'Туристичне страхування'])
                        </div>
                    </div>
                    <!--additional text?-->



                </div>
                <!-- END: car-insurance -->

            </div>
            <!-- END: col-sm-9 -->

            <!-- BEGIN: sidebar__block -->
            <!-- END: sidebar__block -->
            <script type="text/javascript">
                function changeWantEmail() {
                    if ($("#form_kasko_phone #wantemail").prop('checked')) {
                        $("#form_kasko_phone #email").attr('disabled', false);
                        $("#form_kasko_phone #time1").attr('disabled', 'disabled');
                        $("#form_kasko_phone #time2").attr('disabled', 'disabled');
                    } else {
                        $("#form_kasko_phone #email").attr('disabled', 'disabled');
                        $("#form_kasko_phone #time1").attr('disabled', false);
                        $("#form_kasko_phone #time2").attr('disabled', false);
                    }
                }
                function openOtherFields() {
                    if ($("#other_fields:hidden").length>0) {
                        $("#add_fields").val('1');
                        $("#other_fields").show();
                        $("#show_fields").html("<i class='fa fa-chevron-up'></i> Сховати необов'язкові поля");
                    } else {
                        $("#add_fields").val('0');
                        $("#other_fields").hide();
                        $("#show_fields").html("<i class='fa fa-chevron-down'></i> Показати необов'язкові поля");
                    }
                }
                function selectDate() {
                    if ($("#form_kasko_phone #time1").val() == 0) {
                        $("#form_kasko_phone #time2").hide();
                        $("#form_kasko_phone #time1").addClass("width-165");
                    } else {
                        $("#form_kasko_phone #time2").show();
                        $("#form_kasko_phone #time1").removeClass("width-165");
                    }
                }
            </script>
            <script type="text/javascript">
                $(document).ready(function() {
                    $("#form_kasko_phone #phone").mask("+38(999)999-9999");
                });
            </script>



            <!-- BEGIN: sidebar__block -->
            <!-- END: sidebar__block -->
            </aside>
        </div>
        <!-- END: row -->
    </div>
    <!-- END: content-page -->
@stop