@extends('front-layouts.master')

@section('title', 'Автострахування КАСКО')

@section('breadcrumb')
    @include('front-layouts.helpers.breadcrumb', [
        'links' => [
            ['url' => route('front.homepage'), 'name' => 'Главная'],
            ['url' => null, 'name' => 'Автострахування КАСКО']
        ]
    ])
@stop

@section('content')
    <!-- BEGIN: content-page -->
    <div class="content-page">

        <!--<span></span>-->
        <h1>Автострахування КАСКО</h1>




        <!-- BEGIN: content-page__banner -->
        <div class="content-page__banner">
            <img src="/front/assets/content/section/3/3_pict3.jpg" alt="">
        </div>
        <!-- END: content-page__banner -->



        <!-- 2 column layout -->
        <div class="row">
            <div class="col-sm-9">

                <!-- BEGIN: car-insurance -->
                <div class="car-insurance">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="car-insurance__head-text">
                                <p><strong>Добровільне страхування наземного транспорту (КАСКО)</strong> — страхування транспортних засобів від пошкодження, знищення, втрати внаслідок дорожньо-транспортної події, незаконного заволодіння, пожежі, вибуху, стихійного лиха, протиправних дій третіх осіб, зовнішнього впливу сторонніх предметів.</p>
                            </div>
                            @include('front-layouts.pages.helpers.form-feedback', ['typeFeedback' => 'Автострахування КАСКО'])
                        </div>
                    </div>
                    <!--additional text?-->

                </div>
                <!-- END: car-insurance -->

            </div>
            <!-- END: col-sm-9 -->

        </div>
        <!-- END: row -->
    </div>
    <!-- END: content-page -->
@stop