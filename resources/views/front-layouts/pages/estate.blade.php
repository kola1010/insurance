@extends('front-layouts.master')

@section('title', 'Страхування майна')

@section('breadcrumb')
    @include('front-layouts.helpers.breadcrumb', [
        'links' => [
            ['url' => route('front.homepage'), 'name' => 'Главная'],
            ['url' => null, 'name' => 'Страхування майна']
        ]
    ])
@stop

@section('content')
    <!-- BEGIN: content-page -->
    <div class="content-page">

        <!--<span></span>-->
        <h1>Страхування майна</h1>




        <!-- BEGIN: content-page__banner -->
        <div class="content-page__banner">
            <img src="/front/assets/content/section/11/11_pict3.jpg" alt="">
        </div>
        <!-- END: content-page__banner -->



        <!-- 2 column layout -->
        <div class="row">
            <div class="col-sm-9">

                <!-- BEGIN: car-insurance -->
                <div class="car-insurance">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="car-insurance__head-text">
                                <p>Наше житло дає кожному з нас тепло та домашній затишок. Але, в той самий час, потребує захисту. Практичний досвід показує, що навіть в кожному окремому будинку відбувається досить велика кількість пошкоджень та аварій. Що робити, якщо неприємності сталися у сусідів, а постраждало ваше майно? Хто компенсує збитки? А де взяти гроші на купівлю нової квартири в разі її повного пошкодження незалежно від причин: будь то стихійне лихо, аварія чи власна необачність?</p>
                                <p>Страхування будинків та квартир - &nbsp;це реальна необхідність чи розкіш? Звичайно, страхування квартири та відповідальності перед сусідами не врятує квартиру від потопу чи пожежі, але захистить вас від фінансових збитків.</p>
                                <p><strong>Група PZU в Україні пропонує до Вашої уваги різні види страхування майна фізичних осіб, які дозволять Вам не хвилюватися про безпеку вашого майна.</strong></p>
                            </div>
                            @include('front-layouts.pages.helpers.form-feedback', ['typeFeedback' => 'Страхування майна'])
                        </div>
                    </div>
                    <!--additional text?-->



                    <footer class="tab-content__footer"></footer>

                </div>
                <!-- END: car-insurance -->

            </div>
            <!-- END: col-sm-9 -->
        </div>
        <!-- END: row -->
    </div>
    <!-- END: content-page -->
@stop