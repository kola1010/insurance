@extends('front-layouts.master')

@section('title', 'Зворотній зв`язок')

@section('breadcrumb')
    @include('front-layouts.helpers.breadcrumb', [
        'links' => [
            ['url' => route('front.homepage'), 'name' => 'Главная'],
            ['url' => null, 'name' => 'Зворотній зв`язок']
        ]
    ])
@stop

@section('content')
    <!-- BEGIN: content-page -->
    <div class="content-page">

        <!--<span></span>-->
        <h1>Зворотній зв'язок</h1>




        <!-- BEGIN: content-page__banner -->
        <div class="content-page__banner">
            <img src="/front/assets/content/section/298/298_pict3.jpg" alt="">
        </div>
        <!-- END: content-page__banner -->



        <!-- 2 column layout -->
        <div class="row">
            <div class="col-sm-9">
                <div class="main-content">
                    <div class="container-form-range" style="font-size:14px;line-height: 18px;margin-bottom:15px;">

                        <p>Шановні клієнти!</p>

                        <p>Заповнивши наведену нижче форму, ви зможете залишити скаргу щодо якості обслуговування.<br />
                            Зверніть увагу, компанія не надає інформацію щодо страхових випадків третім особам.</p>

                        <form class="form-range new_form" id="form_question" method="POST" name="form_question" action="{{ route('front.feedbackSubmit') }}">
                            @csrf
                            <input type="hidden" name="type" value="Обратная связь">
                            <input type="hidden" name="lang" value="1" />
                            <div class="row">
                                <div class="col-lg-7 col-md-8 col-sm-10">
                                    <p class="form-group" id="group-name">
                                        <label class="control-label" for="name">Ім'я та Прізвище*:</label>
                                        <input class="form-control" type="text" required value="" name="name" id="name">
                                        <span class="invalid-feedback">* Обов’язкове поле</span>
                                    </p>
                                    <p class="form-group" id="group-phone">
                                        <label class="control-label" for="phone">Телефон*:</label>
                                        <input class="form-control" type="text" required value="" name="phone" id="phone" placeholder="В такому форматі: +38 000 000 00 00">
                                        <span class="invalid-feedback">* Обов’язкове поле</span>
                                    </p>
                                    <p class="form-group" id="group-email">
                                        <label class="control-label" for="email">Е-mail*:</label>
                                        <input class="form-control" type="email" required value="" name="email" id="email">
                                        <span class="invalid-feedback">* Обов’язкове поле</span>
                                    </p>
                                    <div class="form-group" id="group-agree">
                                        <label class="custom-checkbox">
                                            Я даю згоду на обробку моїх персональних даних
                                            <input type="checkbox" checked="checked" name="agree" id="agree">
                                            <span class="checkmark"></span>
                                        </label>
                                        <span class="invalid-feedback">* Обов’язкове поле</span>
                                    </div>
                                    <div class="form-group">
                                        <button type="button" id="btn-cancel" class="btn btn-cancel">Скасувати</button>
                                        <button type="submit" class="btn">Відправити</button>
                                    </div>
                                    <div class="form-group" style="margin-top: 40px;">
                                        <p style="font-size: 12px;"><i>*</i><span> поля обов’язкові для заповнення</span></p>
                                    </div>
                                </div>
                                <div class="col-lg-5 col-md-4 col-sm-2">

                                </div>

                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <aside class="col-sm-3 sidebar">
                <!-- BEGIN: sidebar__block -->

                <div class='bottom-2'><div class="box">
                        <div class="font-3">Додатково рекомендуємо</div>
                        <ul class="list-2">
                            <li><a href="javascript:void(0);">Інтернет-магазин</a></li>
                            <li><a href="javascript:void(0);">SOS-сервіс Україна</a></li>
                            <li><a href="javascript:void(0);">Мапа регіональних офісів</a></li>
                            <li><a href="javascript:void(0);">Ваша думка</a></li>
                        </ul>
                    </div></div>

                <!-- END: sidebar__block -->
            </aside>
        </div>
    </div>
    <script type="text/javascript">
        $(document).ready(function() {
            $("#form_question #phone").mask("+38 999 999 99 99");
            $("#insurance").on('change', function () {
                if ($(this).val() == 0) {
                    $('#policy').parent().hide();
                } else {
                    $('#policy').parent().show();
                }
            });
            $('#city').on('change', function () {
                getFilialsByCity($(this).val(), 'form_question');
            });
            $('#btn-cancel').on('click', function () {
                $('#form_question').trigger('reset');
                $('#life1').prop('checked', false);
                $('#life2').prop('checked', false);
                $('input[name="life"]').parent().removeClass('active');
                $('#insurance').trigger("chosen:updated");
                $('#policy').parent().hide();
                $('#city').trigger("chosen:updated");
                getFilialsByCity(0, 'form_question');
                $('#filial').attr('disabled', 'disabled');
                $('#filial').trigger("chosen:updated");
                $('#category').trigger("chosen:updated");
                ajax_query('default', 'selectlife', { 'value': 0, 'lang': '1'});
            });
            $('#life1').parent().on('click', function () {
                ajax_query('default', 'selectlife', { 'value': 1, 'lang': '1'});
            });
            $('#life2').parent().on('click', function () {
                ajax_query('default', 'selectlife', { 'value': 2, 'lang': '1'});
            });
    //        $('#exampleFormControlFile1').multifile();
            var $input = $('#add_files');
            if ($input.length) {
                $input.fileinput({
                    language: 'uk',
                    allowedFileExtensions: ['pdf', 'doc', 'docx', 'xls', 'xlsx', 'jpg', 'jpeg', 'png', 'rar', 'zip'],
    //                showCaption: false,
                    showRemove: false,
                    showUpload: false,
                    showUploadStats: false,
                    showUploadedThumbs: false,
                    showCancel: false,
                    browseIcon: '',
                    'theme': 'explorer-fas',
                    'uploadUrl': '/uploadfiles.php',
                    overwriteInitial: false,
                    fileActionSettings: {
                        removeTitle: 'Видалити файл'
                    },
                    layoutTemplates: {
                        progress: '&nbsp;',
                        actions: '<div class="file-actions">\n' +
                        '    <div class="file-footer-buttons">\n' +
                        "        {delete}" +
                        '    </div>\n' +
                        "    {drag}\n" +
                        '' +
                        '    <div class="clearfix"></div>\n' +
                        '</div>'
                    }
                }).on('fileuploaded', function(event, previewId, index, fileId) {
                    var response = previewId['response'];
                    var form = $('#form_question');
                    var inp = $('<input/>', {
    //                    'id': 'filename'+fileId,
                        'type': 'hidden',
                        'name': 'file_names[]',
                        'class': 'upload-files',
                        'value': response['fileName']+';'+response['filePath'],
                    });
                    form.append(inp);
                    var input_count = $('.upload-files').length;
                    var fc = $('#file_count').val();
                    if (fc == input_count) {
                        setTimeout(function () {
                            ajax_query('default', 'sendquestion', document.forms['form_question']);
                        }, 1000);
                    }
                });
            }
        });
    </script>
@stop