<!doctype html>
<html lang="ua">
<head>
    <meta charset="utf-8">
    <title>@yield('title')</title>
    <meta name="title" content="Страхова компанія ПЗУ Україна ᐈ Купити страховий поліс"/>
    <meta name="keywords" content=""/>
    <meta name="description" content="ПЗУ - одна з найбільших страхових компаній в Центральній та Східній Європі ✅ Небанківська фінансова Группа ПЗУ Україна одна з найдійніших страховах компаній України"/>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <link rel="alternate" href="index.html" hreflang="uk-ua"/>
    <link rel="alternate" href="ru/index.html" hreflang="ru-ua"/>

    <link rel="stylesheet" href="/front/assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="/front/assets/css/font-awesome.min.css">
    <link rel="stylesheet" href="/front/assets/css/styles.min.css">
    <link rel="stylesheet" href="/front/assets/css/fileinput.css?t=1590493458">
    <link rel="stylesheet" href="/front/assets/css/added.css?t=1590493458">
    <link rel="icon" href="img/favicon.ico" type="image/x-icon">
    <link rel="shortcut icon" href="img/favicon.ico" type="image/x-icon">

    <script src="/front/assets/js/jquery-3.3.1.min.js"></script>
    <!-- Modernizr -->
    <script defer src="/front/assets/js/modernizr.min.js"></script>
    <!--[if lt IE 9]>
    <!--<script defer src="/front/assets/js/html5shiv.min.js"></script>-->
    {{--<script defer src="/front/assets/js/respond.min.js"></script>--}}
    <link rel="stylesheet" href="/front/assets/css/ie8.css">
    <![endif]-->
    <meta name="google-site-verification" content="Yxr5DylmtK7l10U9ROSvk80EuL10f6B_Vfn9VDylSrg"/>
    <meta name='yandex-verification' content='7f8b1d4eeba9e3b2'/>
    {{--<script defer src="/front/assets/js/video.min.js"></script>--}}

    @yield('css')
</head>
<body>


<noscript>
    <iframe src="https://www.pzu.com.ua/www.googletagmanager.com/ns.html?id=GTM-NMRCG8" height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<div class="container">
    <header class="header-page">
        <div class="header-page__row">
            <div class="row">
                <div class="col-lg-6 col-md-6">
                    <div class="header-page__left-block">
                        <div class="logo">
                            <a href="index.html" title="На головну сторiнку">
                                <img class="logo-main" src="/front/assets/img/logo-en.png" alt="Страхова компанія PZU Україна">
                                <img class="logo-mobile" src="/front/assets/img/logo-en.png" alt="Страхова компанія PZU Україна">
                            </a>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6 col-md-6">
                    <div class="header-page__right-block">
                        <div class="collapse navbar-collapse" id="js-navbar-collapse">
                            <ul class="nav navbar-nav">
                                <li class="dropdown">
                                    <a class="dropdown-toggle" href="{{ route('front.aboutUs') }}">Про нас</a>
                                </li>
                                <li><a href="{{ route('front.contacts') }}">Контакти</a></li>
                                <li><a href="{{ route('front.feedback') }}">Зворотній зв'язок</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </header>

    @yield('breadcrumb')
    @yield('content')

<!-- BEGIN: footer-page -->
<footer class="footer-page footer-page_main-page">
    <div class="row">
        <div class="col-sm-4 col-xs-6">
            <a href="company.html">Про компанію</a>
        </div>
        <div class="col-sm-4 col-xs-6">
            <a href="contacts.html">Контакти</a>
        </div>
        <div class="col-sm-4 col-xs-6">
            <a href="questions.html">Зворотній зв'язок</a>
        </div>
    </div>
    <div class="row footer-page__copyright">
        <div class="col-sm-9 social-footer">
            <div id="fb-root"></div>
            <script>(function(d, s, id) {
                    var js, fjs = d.getElementsByTagName(s)[0];
                    if (d.getElementById(id)) return;
                    js = d.createElement(s); js.id = id;
                    js.src = "//connect.facebook.net/ua_UA/sdk.js#xfbml=1&version=v2.4";
                    fjs.parentNode.insertBefore(js, fjs);
                }(document, 'script', 'facebook-jssdk'));</script>
            <div class="fb-like" data-href="https://www.facebook.com/pg/hru.it/posts/" data-layout="button_count" data-action="like" data-show-faces="true" data-share="true"  style="float:left;margin-top:3px;margin-right:5px;"></div>

            <script src="/front/assets/js/downloaded/google_platform.js"></script>
            <div class="g-ytsubscribe" data-channelid="UC8butISFwT-Wl7EV0hUK0BQ" data-layout="default" data-count="default"></div>
        </div>
    </div>
</footer>
<!-- END: footer-page -->

<input type="hidden" id="lang" value="1">

</div>

<button class="btn hide" id="btn-modal-consult" data-toggle="modal" data-target="#modal_form_kasko">for modal</button>
<!-- BEGIN: modal -->
<div id="modal_form_kasko" class="modal fade" data-backdrop="true" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="background-color: rgba(0, 0, 0, 0);top:0;bottom:0;left:0;width: 100%;margin: 0;">
    <div class="modal-dialog modal-big" style="background-color: #fff;border-radius: 6px;left:0;">
        <div class="modal-header">
            <h3 style="text-align:center;">Прошу надати мені консультацію
                щодо <b id="insurance_name">страхування</b>
                <button type="button" id="btn-close-modal" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </h3>
        </div>
        <div class="modal-body">
            <form onsubmit="return false" name="form_kasko" id="form_kasko" class="form-kasko" enctype="multipart/form-data" style="margin:0;">

                <input type="hidden" name="lang" value="1">
                <input type="hidden" name="product" id="product" value="1">
                <input type="hidden" name="by_product" id="by_product" value="страхуванню">
                <input type="hidden" name="filial_require" id="filial_require" value="1">

                <input type="hidden" name="isset_choose_product" id="isset_choose_product" value="1">
                <div id="choose_product">
                    <div class="row form-group">
                        <div class="col-sm-3 col-xs-6" style="text-align: left;">
                            <label class="control-label label-head">Автомобіля</label>
                            <label class="label-choose" for="prod_4">
                                <input type="checkbox" name="choose_prod[]" id="prod_4" value="4"><div>КАСКО</div></label>
                            <label class="label-choose" for="prod_17">
                                <input type="checkbox" name="choose_prod[]" id="prod_17" value="17"><div>Автоцивілка</div></label>
                            <label class="label-choose" for="prod_18">
                                <input type="checkbox" name="choose_prod[]" id="prod_18" value="18"><div>Зелена картка</div></label>
                            <label class="label-choose" for="prod_203">
                                <input type="checkbox" name="choose_prod[]" id="prod_203" value="203"><div>Автозахист</div></label>
                            <label class="label-choose" for="prod_19">
                                <input type="checkbox" name="choose_prod[]" id="prod_19" value="19"><div>Нещасний випадок</div></label>
                        </div>
                        <div class="col-sm-3 col-xs-6" style="text-align: left;">
                            <label class="control-label label-head">Майна</label>
                            <label class="label-choose" for="prod_21">
                                <input type="checkbox" name="choose_prod[]" id="prod_21" value="21"><div>Квартира. Будинок. Дача</div></label>
                            <label class="label-choose" for="prod_22">
                                <input type="checkbox" name="choose_prod[]" id="prod_22" value="22"><div>Small business</div></label>
                        </div>
                        <div class="col-sm-3 col-xs-6" style="text-align: left;">
                            <label class="control-label label-head">Родини</label>
                            <label class="label-choose" for="prod_14">
                                <input type="checkbox" name="choose_prod[]" id="prod_14" value="14"><div>Страхування життя</div></label>
                            <label class="label-choose" for="prod_12">
                                <input type="checkbox" name="choose_prod[]" id="prod_12" value="12"><div>Туристичне страхування</div></label>
                            <label class="label-choose" for="prod_118">
                                <input type="checkbox" name="choose_prod[]" id="prod_118" value="118"><div>Страхування студентів за кордоном</div></label>
                        </div>
                        <div class="col-sm-3 col-xs-6" style="text-align: left;">
                            <label class="control-label label-head">Інше</label>
                            <label class="label-choose" for="prod_0">
                                <input type="checkbox" name="choose_prod[]" id="prod_0" value="0"><div>Інші продукти</div></label>
                        </div>
                    </div>
                    <p class="form-group error" id="error_choose_prod" style="display: none;">
                        <label class="control-label" >Виберіть хоча б один продукт!</label>
                    </p>
                </div>
                <div style="border-bottom: 1px solid #eee;margin-bottom: 15px;"></div>
                <div id="avto_info" style="display:none;">
                    <input type="hidden" name="catvehicle" id="catvehicle" value="0">
                    <input type="hidden" name="marka" id="marka" value="">
                    <input type="hidden" name="model" id="model" value="">
                    <input type="hidden" name="prodyear" id="prodyear" value="0">
                    <input type="hidden" name="mileage" id="mileage" value="0">
                    <input type="hidden" name="driverage" id="driverage" value="0">
                    <input type="hidden" name="driverexper" id="driverexper" value="0">
                    <div class="row">
                        <div class="col-sm-6 col-xs-6 left-col">Категорія транспортного засобу</div>
                        <div class="col-sm-6 col-xs-6 right-col" id="catvehicle_text"></div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6 col-xs-6 left-col">Марка та модель авто</div>
                        <div class="col-sm-6 col-xs-6 right-col" id="marka_text"></div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6 col-xs-6 left-col">Рік випуску та пробіг, км</div>
                        <div class="col-sm-6 col-xs-6 right-col" id="prodyear_text"></div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6 col-xs-6 left-col">Вік та стаж водіїв</div>
                        <div class="col-sm-6 col-xs-6 right-col" id="driverage_text"></div>
                    </div>
                </div>
                <p class="form-group" id="block_nf_name">
                    <label class="control-label" for="name">Моє ім'я *</label>
                    <input class="form-control focus_noerror" type="text" value="" name="name" id="name">
                </p>
                <div class="row">
                    <div class="col-sm-6">
                        <p class="form-group" id="block_nf_phone">
                            <label class="control-label" for="phone">Телефон *</label>
                            <input class="form-control focus_noerror" type="text" value="" name="phone" id="phone">
                        </p>
                        <p class="form-group" id="block_nf_city">
                            <label class="control-label" for="city" style="line-height:16px;margin:34px 0px 4px 0px;">Моє місто проживання або найближче до мене місто *</label>
                            <select class="form-control focus_noerror" name="city" id="city" onchange="getFilialsByCity(this.value,'form_kasko', 0)">
                                <option value="">Оберіть місто</option>
                                <option value="onlineshop">Без міста – інтернет-магазин</option>                            <option value="1">Київ</option>
                                <option value="52">Львів</option>
                                <option value="42">Харків</option>
                                <option value="34">Одеса</option>
                                <option value="15">Дніпро</option>
                                <option value="11">Луцьк</option>
                                <option value="73">Бахмут</option>
                                <option value="23">Бердянськ</option>
                                <option value="26">Біла Церква</option>
                                <option value="91">Болград</option>
                                <option value="28">Бровари</option>
                                <option value="29">Васильків</option>
                                <option value="75">Вишневе</option>
                                <option value="10">Вінниця</option>
                                <option value="14">Володимир-Волинський</option>
                                <option value="66">Глухів</option>
                                <option value="53">Городок</option>
                                <option value="15">Дніпро</option>
                                <option value="57">Дрогобич</option>
                                <option value="18">Житомир</option>
                                <option value="82">Жовті Води</option>
                                <option value="21">Запоріжжя</option>
                                <option value="2">Івано-Франківськ</option>
                                <option value="30">Ірпінь</option>
                                <option value="94">Калинівка</option>
                                <option value="46">Кам'янець-Подільский</option>
                                <option value="51">Кам’янське</option>
                                <option value="1">Київ</option>
                                <option value="67">Конотоп</option>
                                <option value="55">Краковець</option>
                                <option value="64">Краматорськ</option>
                                <option value="37">Кременчук</option>
                                <option value="16">Кривий Ріг</option>
                                <option value="25">Кропивницький</option>
                                <option value="11">Луцьк</option>
                                <option value="52">Львів</option>
                                <option value="12">Любомль</option>
                                <option value="4">Маріуполь</option>
                                <option value="22">Мелітополь</option>
                                <option value="32">Миколаїв</option>
                                <option value="20">Мукачеве</option>
                                <option value="45">Нетішин</option>
                                <option value="98">Ніжин</option>
                                <option value="68">Нікополь</option>
                                <option value="99">Нова Каховка</option>
                                <option value="81">Новоград-Волинський</option>
                                <option value="97">Новомосковськ</option>
                                <option value="70">Новопсков</option>
                                <option value="27">Обухів</option>
                                <option value="34">Одеса</option>
                                <option value="17">Павлоград</option>
                                <option value="33">Первомайськ</option>
                                <option value="36">Полтава</option>
                                <option value="65">Прилуки</option>
                                <option value="54">Рава-Руська</option>
                                <option value="85">Радехів</option>
                                <option value="38">Рівне</option>
                                <option value="78">Рубіжне</option>
                                <option value="76">Сєвєродонецьк</option>
                                <option value="96">Сміла</option>
                                <option value="93">смт. Куликів</option>
                                <option value="60">Сокільники</option>
                                <option value="92">Соснівка</option>
                                <option value="74">Старобільськ</option>
                                <option value="77">Стрий</option>
                                <option value="39">Суми</option>
                                <option value="40">Тернопіль</option>
                                <option value="19">Ужгород</option>
                                <option value="71">Умань</option>
                                <option value="13">Устилуг</option>
                                <option value="42">Харків</option>
                                <option value="43">Херсон</option>
                                <option value="44">Хмельницький</option>
                                <option value="58">Червоноград</option>
                                <option value="47">Черкаси</option>
                                <option value="48">Чернівці</option>
                                <option value="49">Чернігів</option>
                                <option value="79">Чорноморськ (колишній Іллічівськ )</option>
                                <option value="41">Чортків</option>
                                <option value="63">Южноукраїнськ</option>
                                <option value="56">Яворів</option>
                            </select>
                        </p>
                        <p class="form-group">
                            <a onclick="openOtherFields2()" id="show_fields2" style="cursor:pointer;"><i class="fa fa-chevron-down"></i> Показати необов'язкові поля</a>
                            <input type="hidden" name="add_fields2" id="add_fields2" value="0">
                        </p>
                        <p class="form-group" id="other_fields2" style="display:none;">
                            <label class="control-label" for="time1">Виберіть зручний час для дзвінка</label>
                            <select class="form-control width-165" style="margin-right:10px;width:120px;float:left;" name="time1" id="time1" onchange="selectDate2()">
                                <option value="0">у найближчий час</option>
                                <option value="1">сьогодні</option>
                                <option value="2">завтра</option>
                                <option value="3">післязавтра</option>
                            </select>
                            <select class="form-control" name="time2" id="time2" style="display:none;width:80px;">
                                <option value="09:00">09:00</option>
                                <option value="10:00">10:00</option>
                                <option value="11:00">11:00</option>
                                <option value="12:00">12:00</option>
                                <option value="13:00">13:00</option>
                                <option value="14:00">14:00</option>
                                <option value="15:00">15:00</option>
                                <option value="16:00">16:00</option>
                                <option value="17:00">17:00</option>
                                <option value="18:00">18:00</option>
                            </select>
                        </p>
                    </div>
                    <div class="col-sm-6">
                        <p class="form-group" id="block_nf_email">
                            <label class="control-label" for="email">Мій E-mail</label>
                            <input class="form-control focus_noerror" type="text" value="" name="email" id="email" disabled="disabled">
                            <label for="wantemail_m" style="text-align:left;">
                                <input type="checkbox" value="1" name="wantemail_m" id="wantemail_m" onchange="changeWantEmail2()">
                                Хочу, щоб мені написали
                            </label>
                        </p>
                        <p class="form-group" id="block_nf_filial">
                            <label class="control-label" for="filial">Найближчий підрозділ PZU у моєму місті *</label>
                            <select class="form-control focus_noerror" name="filial" id="filial">
                                <option value="">Виберіть філіал</option>

                            </select>
                        </p>
                        <p class="form-group" id="other_fields3" style="display:none;">
                            <label class="control-label" for="comment">Коментар</label>
                            <textarea class="form-control focus_noerror" type="text" value="" name="comment" id="comment"></textarea>
                        </p>
                        <p class="form-group">
                            <small>* - поля обов’язкові для заповнення</small>
                        </p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <p class="form-group" id="p_agree">
                            <label for="agree" style="text-align:left;font-size: 13px;line-height: 16px;">
                                <input type="checkbox" value="1" name="agree" id="agree" checked="checked">
                                Я даю згоду на обробку моїх персональних даних, наданих у даній формі – заявці на консультацію.
                            </label>
                        </p>
                    </div>
                </div>
            </form>
        </div>
        <div class="modal-footer" style="text-align:center;">
            <button class="btn" style="line-height:18px;padding: 5px 18px;" id="submit_modal_consultation">Замовити консультацію</button>
            <!--<a data-dismiss="modal" aria-hidden="true" style="cursor:pointer;color:#8f8584;" class="" id="btn-modal-close">Закрити</a>-->
        </div>
    </div>
</div>
<script type="text/javascript">
    function changeWantEmail2() {
        if ($("#form_kasko #wantemail_m").prop('checked')) {
            $("#form_kasko #email").attr('disabled', false);
            $("#form_kasko #time1").attr('disabled', 'disabled');
            $("#form_kasko #time2").attr('disabled', 'disabled');
        } else {
            $("#form_kasko #email").attr('disabled', 'disabled');
            $("#form_kasko #time1").attr('disabled', false);
            $("#form_kasko #time2").attr('disabled', false);
        }
    }
    function openOtherFields2() {
        if ($("#other_fields2:hidden").length>0) {
            $("#add_fields2").val('1');
            $("#other_fields2").show();
            $("#other_fields3").show();
            $("#show_fields2").html("<i class='fa fa-chevron-up'></i> Сховати необов'язкові поля");
        } else {
            $("#add_fields2").val('0');
            $("#other_fields2").hide();
            $("#other_fields3").hide();
            $("#show_fields2").html("<i class='fa fa-chevron-down'></i> Показати необов'язкові поля");
        }
    }
    function selectDate2() {
        if ($("#form_kasko #time1").val() == 0) {
            $("#form_kasko #time2").hide();
        } else {
            $("#form_kasko #time2").show();
        }
    }
    $(document).ready(function() {
        $("#form_kasko #phone").mask("+38(999)999-9999");
    });
</script>
<!-- END: modal -->
<button class="btn hide" id="btn-modal" data-toggle="modal" data-target="#myModal">for modal</button>
<div id="myModal" class="modal fade" data-backdrop="true" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
     aria-hidden="true">
    <!--<div class="modal-header">
      <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
    </div>-->
    <div class="modal-body">
        <h3 style="margin:20px 0px;" id="myModalText">Modal header</h3>
    </div>
    <div class="modal-footer">
        <a style="cursor:pointer;color:#8f8584;display:none;" class="" id="btn-modal-close-reload">Закрити</a>
        <a data-dismiss="modal" aria-hidden="true" style="cursor:pointer;color:#8f8584;" class=""
           id="btn-modal-close">Закрити</a>
    </div>
</div>
<!-- END: modal -->

<script>
    $(document).ready(function () {
        $(".focus_noerror").on('focus', function (e) {
            $("#block_" + $(this).attr('name')).removeClass('error');
            $("#block_nf_" + $(this).attr('name')).removeClass('error');
        });

        $("#js-tabs > ul").find('a').on('click', function () {
            openTab('tab-' + $(this).attr("data-target"));
        });
    });

    function openTab(name) {
        // $("a[data-name='"+name+"']").trigger('click');
        $("li[role='presentation']").removeClass('active');
        $("a[data-name='" + name + "']").parent().addClass('active');
        $(".tab-pane").removeClass("fade in active");
        $("#" + name).addClass("fade in active");
        $('html, body').animate({ scrollTop: $("#js-tabs").offset().top - 25}, 500);
    }
</script>

<script defer src="/front/assets/js/jquery.maskedinput.min.js"></script>

<script src="/front/assets/js/bootstrap.min.js"></script>
<script defer src="/front/assets/js/jquery.bootstrap-responsive-tabs.min.js"></script>
<script defer src="/front/assets/js/jquery.easing.1.3.min.js"></script>
<script defer src="/front/assets/js/jquery.bxslider.js"></script>
<script defer src="/front/assets/js/jquery.marquee.min.js"></script>
<script defer src="/front/assets/js/jquery.magnific-popup.min.js"></script>
<script defer src="/front/assets/js/JsHttpRequest.js"></script>
<script src="/front/assets/js/custom_select/chosen.jquery.js?v=19.09170704"></script>
<script src="/front/assets/js/public.js?v=19.09170704"></script>
<script src="/front/assets/js/bootstrap-filestyle.min.js"></script>
<script>
    /* filestyle */
    $(":file").filestyle({
        buttonText: 'Виберiть файл'
    });
</script>

<script>
    @if(Session::has('success'))
        alert('{{ Session::get('success') }}')
    @elseif(Session::has('error'))
        alert('{{ Session::get('error') }}')
    @endif

    @if($errors->any())
        @foreach($errors as $error)
            alert('{{ $error }}');
        @endforeach
    @endif
</script>


<script src="/front/assets/js/custom.min.js"></script>
<script>
    /* ========== jQuery ========== */
    (function ($) {
        $(function () {
            /* ========== video youtube ========== */
            if ($('a.video__link').length > 0) {
                $('a.video__link').magnificPopup({
                    disableOn: 310,
                    type: 'iframe',
                    mainClass: 'mfp-fade',
                    removalDelay: 160,
                    preloader: false,
                    fixedContentPos: false
                });
            }

            /* ========== landing faq ========== */
            function landingFaq() {
                var landingItem = $('li.landing-faq__item'),
                    landingButton = $('button.landing-faq__item-title'),
                    landingPanel = $('div.landing-faq__item-content');

                landingPanel.hide();
                landingItem.eq(0).find('div.landing-faq__item-content').show().end().find('button.landing-faq__item-title').addClass('landing-faq__item-title--open');

                landingButton.on('click', function () {
                    landingPanel.not($(this).next()).slideUp();
                    landingButton.not($(this)).removeClass('landing-faq__item-title--open');
                    $(this).toggleClass('landing-faq__item-title--open').next().slideToggle();
                });
            }

            landingFaq();

        }); // end DOM ready
    })(jQuery); // end jQuery

    $(function () {
        $("#li_tab1").click(function (event) {
            event.preventDefault();
            $("#instr").hide()
        });
        $("#li_tab2").click(function (event) {
            event.preventDefault();
            $("#instr").show()
        });
    });

    function sendGA(category, action) {
        ga('send', 'event', { eventCategory: category, eventAction: action});
    }
</script>

@yield('scripts')
</body>

</html>
