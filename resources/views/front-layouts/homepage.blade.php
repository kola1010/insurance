@extends('front-layouts.master')

@section('title', 'Страхова компанія ПЗУ Україна ᐈ Купити страховий поліс')

@section('content')
    <!-- BEGIN: main-slider -->
    <div class="main-slider" style="margin-top: 30px;">
        <ul class="bxslider-main" id="js-main-slider">
            <li>
                <img src="/front/assets/content/slide/10/distance-1140x303-2020_04_221.jpg" alt="img">
                <div class="bx-caption bx-caption-right">
                    <a href="{{ route('front.contacts') }}" title="детальніше">детальніше</a>
                </div>
            </li>
            <li>
                <img src="/front/assets/content/slide/10/tourism-web-1140x303-2019_10_204.jpg" alt="img">
                <div class="bx-caption bx-caption-right">
                    <a href="{{ route('front.pages.travel') }}" title="детальніше">детальніше</a>
                </div>
            </li>
            <li>
                <img src="/front/assets/content/slide/10/kasko-assistance-1140x303-2019_06_162.jpg" alt="img">
                <div class="bx-caption bx-caption-right">
                    <a href="{{ route('front.pages.auto') }}" title="детальніше">детальніше</a>
                </div>
            </li>
        </ul>
    </div>
    <!-- END: main-slider -->
    <!-- BEGIN: features -->
    <div class="features">

        <div class="row">
            <div class="col-md-4 col-sm-6 col-xs-6">
                <div class="features__item">
                    <a class="features__title" href="{{ route('front.pages.estate') }}">
                        Страхування майна
                        <figure>
                            <img src="/front/assets/content/settblock/1_pict.jpg" alt="">
                            <div class="features__switch">
                                <button class="features__btn" type="button">
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                </button>
                                <i class="glyphicon glyphicon-remove"></i>
                            </div>
                        </figure>
                    </a>
                </div>
            </div>
            <div class="col-md-4 col-sm-6 col-xs-6">
                <div class="features__item">
                    <a class="features__title" href="{{ route('front.pages.auto') }}">
                        Автострахування КАСКО
                        <figure>
                            <img src="/front/assets/content/settblock/2_pict.jpg" alt="">
                            <div class="features__switch">
                                <button class="features__btn" type="button">
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                </button>
                                <i class="glyphicon glyphicon-remove"></i>
                            </div>
                        </figure>
                    </a>
                </div>
            </div>
            <div class="col-md-4 col-sm-6 col-xs-6">
                <div class="features__item">
                    <a class="features__title" href="{{ route('front.pages.travel') }}">
                        Туристичне страхування
                        <figure>
                            <img src="/front/assets/content/settblock/3_pict.jpg" alt="">
                            <div class="features__switch">
                                <button class="features__btn" type="button">
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                </button>
                                <i class="glyphicon glyphicon-remove"></i>
                            </div>
                        </figure>
                    </a>
                </div>
            </div>
        </div>
        <!-- END: row -->
    </div>
    <!-- END: features -->
@stop