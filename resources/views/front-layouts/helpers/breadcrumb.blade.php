@if($links)
    <div class="breadcrumb" style="margin-top: 20px;">
        @foreach($links as $link)
            <a href="{{ $link['url'] !== null ? $link['url'] : 'javascript:void(0);' }}">{{ $link['name'] }}</a>
        @endforeach
    </div>
@endif