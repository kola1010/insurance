@extends('admin-layouts.master')

@section('title', 'Сотрудники филиала - ' . $branch->name)

@section('content')
    <div class="container">
        <div class="py-5 text-center">
            <h2>Сотрудники филиала - {{ $branch->name }}</h2>
        </div>

        <div class="row">
            <div class="col-md-12 text-right mb-5">
                <a href="{{ route('admin.branch-workers.create', ['branch_id' => $branch->id]) }}" class="btn btn-primary">Добавить сотрудника</a>
            </div>
        </div>
        <div class="row">
            <table class="table">
                <thead class="thead-dark">
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">ФИО</th>
                    <th scope="col">Номер телефона</th>
                    <th scope="col">Емайл</th>
                    <th scope="col"></th>
                </tr>
                </thead>
                <tbody>
                @if(count($branch->workers))
                    @foreach($branch->workers as $worker)
                        <tr>
                            <th scope="row">{{ $worker->worker->id }}</th>
                            <td>{{ $worker->worker->last_name . ' ' . $worker->worker->first_name }}</td>
                            <td>{{ $worker->worker->phone ?? 'Не определено' }}</td>
                            <td>{{ $worker->worker->email ?? 'Не определено' }}</td>
                            <td>
                                <form action="{{ route('admin.branch-workers.destroy', $worker) }}" method="POST" onsubmit="return confirm('Вы действительно хотите удалить данного сотрудника?');">
                                    @csrf
                                    @method('DELETE')
                                    <input type="hidden" name="branch_id" value="{{ $branch->id }}">
                                    <button class="btn btn-danger">Удалить</button>
                                </form>
                            </td>
                        </tr>
                    @endforeach
                @else
                    <tr>
                        <td colspan="5">Сотрудников не найдено</td>
                    </tr>
                @endif
                </tbody>
            </table>
        </div>
    </div>
@stop