<div class="row">
    <div class="col-md-6 mb-3">
        <label for="name">Выберите сотрудника</label>
        <select class="form-control" name="worker_id" id="">
            <option value="" disabled>Выберите</option>
            @if (count($workers))
                @foreach ($workers as $worker)
                    <option value="{{ $worker->id }}">{{ $worker->last_name . ' ' . $worker->first_name }}</option>
                @endforeach
            @endif
        </select>
        <input type="hidden" name="branch_id" value="{{ $branch->id ?? 0 }}">
    </div>
</div>
