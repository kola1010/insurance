@extends('admin-layouts.master')

@section('title', 'Добавление сотрудника в филиал - ' . $branch->name)

@section('content')
    <div class="container">
        <div class="py-5 text-center">
            <h2>Добавление сотрудника в филиал - {{ $branch->name }}</h2>
        </div>

        <div class="row">
            <div class="col-md-12 order-md-1">
                <form class="needs-validation" autocomplete="off" method="POST" action="{{ route('admin.branch-workers.store') }}">
                    @csrf
                    @include('admin-layouts.branch-workers.fields')
                    <hr class="mb-4">
                    <div class="row">
                        <div class="col-md-6">
                            <button class="btn btn-primary btn-lg btn-block" type="submit">Добавить сотрудника в филиал</button>
                        </div>
                        <div class="col-md-6">
                            <a href="{{ route('admin.branch-workers.show', $branch->id) }}" class="btn btn-warning btn-lg btn-block">Назад</a>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@stop

@section('scripts')
    <script>
        $(document).ready(function() {
            @if ($errors->any())
            @foreach ($errors->all() as $error)
            toastr.error('{{ $error }}');
            @endforeach
            @endif
        });
    </script>
@stop