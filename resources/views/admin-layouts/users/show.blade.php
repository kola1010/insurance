@extends('admin-layouts.master')

@section('title', 'Пользователь #' . $user->id)

@section('content')
    <div class="container">
        <div class="py-5 text-center">
            <h2>Редактирование информации о пользователе</h2>
        </div>

        <div class="row">
            <div class="col-md-12 order-md-1">
                <form class="needs-validation" autocomplete="off" method="POST" action="{{ route('admin.users.update', $user) }}">
                    @csrf
                    @method('PUT')
                    @include('admin-layouts.users.fields')
                    <hr class="mb-4">
                    <div class="row">
                        <div class="col-md-6">
                            <button class="btn btn-primary btn-lg btn-block" type="submit">Обновить пользователя</button>
                        </div>
                        <div class="col-md-6">
                            <a href="{{ route('admin.user-documents.show', $user) }}" class="btn btn-warning btn-lg btn-block">Документы пользователя</a>
                        </div>
                    </div>
                </form>
                <form action="{{ route('admin.users.destroy', $user) }}" method="POST" onsubmit="return confirm('Вы действительно хотите удалить данного пользователя?');">
                    @csrf
                    @method('DELETE')
                    <button class="btn btn-danger mt-2 btn-lg btn-block" type="submit">Удалить пользователя</button>
                </form>
            </div>
        </div>
    </div>
@stop

@section('scripts')
    <script>
        $(document).ready(function() {
            @if ($errors->any())
                @foreach ($errors->all() as $error)
                    toastr.error('{{ $error }}');
                @endforeach
            @endif
        });
    </script>
@stop