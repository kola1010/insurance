<div class="row">
    <div class="col-md-6 mb-3">
        <label for="name">Имя</label>
        <input type="text" class="form-control" value="{{ old('first_name', $user->first_name ?? '') }}" name="first_name" id="name" placeholder="">
    </div>
    <div class="col-md-6 mb-3">
        <label for="price">Фамилия</label>
        <input type="text" class="form-control" name="last_name" id="price" placeholder="" value="{{ old('last_name', $user->last_name ?? '') }}">
    </div>
    <div class="col-md-6 mb-3">
        <label for="price">Номер телефона</label>
        <input type="text" class="form-control" name="phone" id="price" placeholder="" value="{{ old('phone', $user->phone ?? '') }}">
    </div>
    <div class="col-md-6 mb-3">
        <label for="price">Емайл</label>
        <input type="text" class="form-control" name="email" id="price" placeholder="" value="{{ old('email', $user->email ?? '') }}">
    </div>
    <div class="col-md-6 mb-3">
        <label for="price">Пароль</label>
        <input type="text" class="form-control" name="{{ isset($user) && $user->password ? 'new_password' : 'password' }}" id="price" placeholder="Введите чтобы изменить пароль">
    </div>

    <div class="col-md-6 mb-3">
        <label for="price">Тип пользователя</label>
        <select name="is_admin" class="form-control">
            <option @if(isset($user) && $user->is_admin) selected="selected" @endif value="1">Администратор</option>
            <option @if(isset($user) && !$user->is_admin) selected="selected" @endif value="0">Клиент</option>
        </select>
    </div>
</div>
