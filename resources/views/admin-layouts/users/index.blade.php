@extends('admin-layouts.master')

@section('title', 'Users')

@section('content')
    <div class="container">
        <div class="py-5 text-center">
            <h2>Пользователи</h2>
        </div>

        <div class="row">
            <div class="col-md-12 text-right mb-5">
                <a href="{{ route('admin.users.create') }}" class="btn btn-primary">Добавить пользователя</a>
            </div>
        </div>
        <div class="row">
            <table class="table">
                <thead class="thead-dark">
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Имя</th>
                    <th scope="col">Фамилия</th>
                    <th scope="col">Номер телефона</th>
                    <th scope="col">Email</th>
                </tr>
                </thead>
                <tbody>
                @if(count($users))
                    @foreach($users as $user)
                        <tr onclick="window.location.href = '{{ route('admin.users.show', $user) }}'">
                            <th scope="row">{{ $user->id }}</th>
                            <td>{{ $user->first_name }}</td>
                            <td>{{ $user->last_name ?? 'Не определено' }}</td>
                            <td>{{ $user->phone ?? 'Не определено' }}</td>
                            <td>{{ $user->email ?? 'Не определено' }}</td>
                        </tr>
                    @endforeach
                @else
                    <tr>
                        <td colspan="5">Пользователей не найдено</td>
                    </tr>
                @endif
                </tbody>
            </table>
        </div>
    </div>
@stop