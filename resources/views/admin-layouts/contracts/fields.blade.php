<div class="row">
    <div class="col-md-6 mb-3">
        <label for="name">Выберите филиал</label>
        <select class="form-control" name="branch_id" id="">
            <option value="" disabled>Выберите</option>
            @if (count($branches))
                @foreach ($branches as $branch)
                    <option @if(isset($contract->branch_id) && $contract->branch_id === $branch->id) selected="selected" @endif value="{{ $branch->id }}">#{{ $branch->id .' - ' . $branch->name }}</option>
                @endforeach
            @endif
        </select>
    </div>
    <div class="col-md-6 mb-3">
        <label for="name">Выберите сотрудника</label>
        <select class="form-control" name="admin_id" id="">
            <option value="" disabled>Выберите</option>
            @if (count($workers))
                @foreach ($workers as $worker)
                    <option @if(isset($contract->admin_id) && $contract->admin_id === $worker->id) selected="selected" @endif value="{{ $worker->id }}">{{ $worker->last_name . ' ' . $worker->first_name }}</option>
                @endforeach
            @endif
        </select>
    </div>
    <div class="col-md-6 mb-3">
        <label for="name">Выберите клиента</label>
        <select class="form-control" name="client_id" id="">
            <option value="" disabled>Выберите</option>
            @if (count($clients))
                @foreach ($clients as $client)
                    <option @if(isset($contract->client_id) && $contract->client_id === $client->id) selected="selected" @endif value="{{ $client->id }}">{{ $client->last_name . ' ' . $client->first_name }}</option>
                @endforeach
            @endif
        </select>
    </div>
    <div class="col-md-6 mb-3">
        <label for="name">Выберите тип страхования</label>
        <select class="form-control" name="insurance_type_id" id="">
            <option value="" disabled>Выберите</option>
            @if (count($insuranceTypes))
                @foreach ($insuranceTypes as $type)
                    <option @if(isset($contract->insurance_type_id) && $contract->insurance_type_id === $type->id) selected="selected" @endif value="{{ $type->id }}">{{ $type->type }}</option>
                @endforeach
            @endif
        </select>
    </div>
    <div class="col-md-6 mb-3">
        <label for="name">Статус</label>
        <select class="form-control" name="status" id="">
            <option value="" disabled>Выберите</option>
            <option @if(isset($contract->status) && $contract->status === 1) selected="selected" @endif value="1">Активный</option>
            <option @if(isset($contract->status) && $contract->status === 0) selected="selected" @endif value="0">Не активный</option>
        </select>
    </div>
    <div class="col-md-6 mb-3">
        <label for="">Тариф</label>
        <input type="text" name="tariff" value="{{ old('tariff', $contract->tariff ?? 0) }}" class="form-control">
    </div>
    <div class="col-md-6 mb-3">
        <label for="">Сумма страхования</label>
        <input type="text" name="sum_insured"  value="{{ old('sum_insured', $contract->sum_insured ?? 0) }}" class="form-control">
    </div>
</div>
