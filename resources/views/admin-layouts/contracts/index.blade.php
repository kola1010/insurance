@extends('admin-layouts.master')

@section('title', 'Контракты')

@section('content')
    <div class="container">
        <div class="py-5 text-center">
            <h2>Контракты</h2>
        </div>

        <div class="row">
            <div class="col-md-12 text-right mb-5">
                <a href="{{ route('admin.contracts.create') }}" class="btn btn-primary">Добавить новый контракт</a>
            </div>
        </div>
        <div class="row">
            <table class="table">
                <thead class="thead-dark">
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Сотрудник</th>
                    <th scope="col">Клиент</th>
                    <th scope="col">Тип страхования</th>
                    <th scope="col">Филиал</th>
                    <th scope="col">Статус</th>
                    <th scope="col">Тариф</th>
                    <th scope="col">Сумма</th>
                </tr>
                </thead>
                <tbody>
                @if(count($contracts))
                    @foreach($contracts as $contract)
                        <tr onclick="window.location.href = '{{ route('admin.contracts.show', $contract) }}'">
                            <th scope="row">{{ $contract->id }}</th>
                            <td>{{ $contract->admin ? $contract->admin->first_name . ' ' . $contract->admin->last_name : 'Не определено' }}</td>
                            <td>{{ $contract->client ? $contract->client->first_name . ' ' . $contract->client->last_name : 'Не определено' }}</td>
                            <td>{{ $contract->insurance->type ?? 'Не определено' }}</td>
                            <td>{{ $contract->branch->name ?? 'Не определено' }}</td>
                            <td>{{ $contract->status }}</td>
                            <td>{{ $contract->tariff }}</td>
                            <td>{{ $contract->sum_insured }}</td>
                        </tr>
                    @endforeach
                @else
                    <tr>
                        <td colspan="5">Контракты не найдено</td>
                    </tr>
                @endif
                </tbody>
            </table>
        </div>
    </div>
@stop