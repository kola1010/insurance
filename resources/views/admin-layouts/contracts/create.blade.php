@extends('admin-layouts.master')

@section('title', 'Добавление нового контракта')

@section('content')
    <div class="container">
        <div class="py-5 text-center">
            <h2>Добавление нового контракта</h2>
        </div>

        <div class="row">
            <div class="col-md-12 order-md-1">
                <form class="needs-validation" autocomplete="off" method="POST" action="{{ route('admin.contracts.store') }}">
                    @csrf
                    @include('admin-layouts.contracts.fields')
                    <hr class="mb-4">
                    <button class="btn btn-primary btn-lg btn-block" type="submit">Создать контракт</button>
                </form>
            </div>
        </div>
    </div>
@stop

@section('scripts')
    <script>
        $(document).ready(function() {
            @if ($errors->any())
            @foreach ($errors->all() as $error)
            toastr.error('{{ $error }}');
            @endforeach
            @endif
        });
    </script>
@stop