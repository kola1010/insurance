@extends('admin-layouts.master')

@section('title', 'Документы -> Пользователь #' . $userDocument->user_id)

@section('content')
    <div class="container">
        <div class="py-5 text-center">
            <h2>Редактирование информации о документах пользователя #{{ $userDocument->user_id }}</h2>
        </div>

        <div class="row">
            <div class="col-md-12 order-md-1">
                <form class="needs-validation" autocomplete="off" method="POST" action="{{ route('admin.user-documents.update', $userDocument) }}">
                    @csrf
                    @method('PUT')
                    @include('admin-layouts.user-documents.fields')
                    <hr class="mb-4">
                    <div class="row">
                        <div class="col-md-6">
                            <button class="btn btn-primary btn-lg btn-block" type="submit">Обновить информацию о документах</button>
                        </div>
                        <div class="col-md-6">
                            <a href="{{ route('admin.users.show', $userDocument->user) }}" class="btn btn-warning btn-lg btn-block">Редактировать пользователя</a>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@stop

@section('scripts')
    <script>
        $(document).ready(function() {
            @if ($errors->any())
            @foreach ($errors->all() as $error)
            toastr.error('{{ $error }}');
            @endforeach
            @endif
        });
    </script>
@stop