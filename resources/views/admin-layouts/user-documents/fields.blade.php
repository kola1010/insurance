<div class="row">
    <div class="col-md-4 mb-3">
        <label for="name">Серия паспорта</label>
        <input type="text" class="form-control" value="{{ old('pass_series', $userDocument->pass_series ?? '') }}" name="pass_series" id="name" placeholder="">
    </div>
    <div class="col-md-4 mb-3">
        <label for="price">Код паспорта</label>
        <input type="text" class="form-control" name="pass_code" id="price" placeholder="" value="{{ old('pass_code', $userDocument->pass_code ?? '') }}">
    </div>
    <div class="col-md-4 mb-3">
        <label for="price">Место прописки</label>
        <input type="text" class="form-control" name="pass_address" id="price" placeholder="" value="{{ old('pass_address', $userDocument->pass_address ?? '') }}">
    </div>

</div>
