@extends('admin-layouts.master')

@section('title', 'Фидбек заявки')

@section('content')
    <div class="container">
        <div class="py-5 text-center">
            <h2>Фидбек заявки</h2>
        </div>

        <div class="row">
            <table class="table">
                <thead class="thead-dark">
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Имя клиента</th>
                    <th scope="col">Номер клиента</th>
                    <th scope="col"></th>
                </tr>
                </thead>
                <tbody>
                @if(count($feedbacks))
                    @foreach($feedbacks as $feedback)
                        <tr>
                            <th scope="row">{{ $feedback->id }}</th>
                            <td>{{ $feedback->name }}</td>
                            <td>{{ $feedback->phone ?? 'Не определено' }}</td>
                            <td><a href="tel:{{ $feedback->phone }}">Позвонить</a></td>
                        </tr>
                    @endforeach
                @else
                    <tr>
                        <td colspan="5">Филиалов не найдено</td>
                    </tr>
                @endif
                </tbody>
            </table>

            <div class="paginate">
                @if(count($feedbacks))
                    {{ $feedbacks->links() }}
                @endif
            </div>
        </div>
    </div>
@stop