<div class="row">
    <div class="col-md-6 mb-3">
        <label for="name">Название</label>
        <input type="text" class="form-control" value="{{ old('name', $branch->name ?? '') }}" name="name" id="name" placeholder="">
    </div>
    <div class="col-md-6 mb-3">
        <label for="price">Номер телефона</label>
        <input type="text" class="form-control" name="phone" id="price" placeholder="" value="{{ old('phone', $branch->phone ?? '') }}">
    </div>
    <div class="col-md-12 mb-3">
        <label for="price">Адрес</label>
        <input type="text" class="form-control" name="address" id="price" placeholder="" value="{{ old('address', $branch->address ?? '') }}">
    </div>
</div>
