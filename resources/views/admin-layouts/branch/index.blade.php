@extends('admin-layouts.master')

@section('title', 'Филиалы')

@section('content')
    <div class="container">
        <div class="py-5 text-center">
            <h2>Филиалы</h2>
        </div>

        <div class="row">
            <div class="col-md-12 text-right mb-5">
                <a href="{{ route('admin.branch.create') }}" class="btn btn-primary">Добавить филиал</a>
            </div>
        </div>
        <div class="row">
            <table class="table">
                <thead class="thead-dark">
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Название</th>
                    <th scope="col">Номер телефона</th>
                    <th scope="col">Адрес</th>
                </tr>
                </thead>
                <tbody>
                @if(count($branches))
                    @foreach($branches as $branch)
                        <tr onclick="window.location.href = '{{ route('admin.branch.show', $branch) }}'">
                            <th scope="row">{{ $branch->id }}</th>
                            <td>{{ $branch->name }}</td>
                            <td>{{ $branch->phone ?? 'Не определено' }}</td>
                            <td>{{ $branch->address ?? 'Не определено' }}</td>
                        </tr>
                    @endforeach
                @else
                    <tr>
                        <td colspan="5">Филиалов не найдено</td>
                    </tr>
                @endif
                </tbody>
            </table>
        </div>
    </div>
@stop