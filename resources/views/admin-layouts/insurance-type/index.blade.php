@extends('admin-layouts.master')

@section('title', 'Типы страховок')

@section('content')
    <div class="container">
        <div class="py-5 text-center">
            <h2>Типы страховок</h2>
        </div>

        <div class="row">
            <div class="col-md-12 text-right mb-5">
                <a href="{{ route('admin.insurance-type.create') }}" class="btn btn-primary">Добавить новый тип</a>
            </div>
        </div>
        <div class="row">
            <table class="table">
                <thead class="thead-dark">
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Название</th>
                    <th scope="col"></th>
                </tr>
                </thead>
                <tbody>
                @if(count($insurances))
                    @foreach($insurances as $insurance)
                        <tr onclick="window.location.href = '{{ route('admin.insurance-type.show', $insurance) }}'">
                            <th scope="row">{{ $insurance->id }}</th>
                            <td>{{ $insurance->type }}</td>
                            <td>
                                <form action="{{ route('admin.insurance-type.destroy', $insurance) }}" method="POST" onsubmit="return confirm('Вы действительно хотите удалить даный тип?');">
                                    @csrf
                                    @method('DELETE')
                                    <button class="btn btn-danger">Удалить</button>
                                </form>
                            </td>
                        </tr>
                    @endforeach
                @else
                    <tr>
                        <td colspan="5">Типы не найдены</td>
                    </tr>
                @endif
                </tbody>
            </table>
        </div>
    </div>
@stop