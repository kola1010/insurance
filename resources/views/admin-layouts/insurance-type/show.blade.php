@extends('admin-layouts.master')

@section('title', 'Тип страховки - ' . $insuranceType->type)

@section('content')
    <div class="container">
        <div class="py-5 text-center">
            <h2>Редактирование типа страховки - {{ $insuranceType->type }}</h2>
        </div>

        <div class="row">
            <div class="col-md-12 order-md-1">
                <form class="needs-validation" autocomplete="off" method="POST" action="{{ route('admin.insurance-type.update', $insuranceType) }}">
                    @csrf
                    @method('PUT')
                    @include('admin-layouts.insurance-type.fields')
                    <hr class="mb-4">
                    <div class="row">
                        <div class="col-md-6">
                            <button class="btn btn-primary btn-lg btn-block" type="submit">Обновить тип страховки</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@stop

@section('scripts')
    <script>
        $(document).ready(function() {
            @if ($errors->any())
            @foreach ($errors->all() as $error)
            toastr.error('{{ $error }}');
            @endforeach
            @endif
        });
    </script>
@stop