@extends('admin-layouts.master')

@section('title', 'Добавление нового типа страховки')

@section('content')
    <div class="container">
        <div class="py-5 text-center">
            <h2>Добавление нового типа страховки</h2>
        </div>

        <div class="row">
            <div class="col-md-12 order-md-1">
                <form class="needs-validation" autocomplete="off" method="POST" action="{{ route('admin.insurance-type.store') }}">
                    @csrf
                    @include('admin-layouts.insurance-type.fields')
                    <hr class="mb-4">
                    <div class="row">
                        <div class="col-md-6">
                            <button class="btn btn-primary btn-lg btn-block" type="submit">Добавить новый тип страховки</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@stop

@section('scripts')
    <script>
        $(document).ready(function() {
            @if ($errors->any())
            @foreach ($errors->all() as $error)
            toastr.error('{{ $error }}');
            @endforeach
            @endif
        });
    </script>
@stop