<?php

namespace App\Console\Commands;

use App\Business\Models\UserDocuments;
use App\User;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Hash;

class AddAdmin extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'add:admin {firstName} {email} {password}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = "Add administrator in system.\nExample: php artisan add:admin {firstName} {email} {password}\n";

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        if (User::where(["email" => $email = $this->argument("email")])->count() > 0) {
            die("User with email $email exists\n");
        }

        $user = new User([
            "first_name" => $this->argument("firstName"),
            "email" => $this->argument("email"),
            "password" => Hash::make($this->argument("password"))
        ]);
        $user->is_admin = 1;
        $user->markEmailAsVerified();
        $user->save();

        UserDocuments::create(['user_id' => $user->id]); // документы пользователя - создаем автоматически строку в БД

        echo "New admin created\n";
    }
}
