<?php

namespace App\Business\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;

class Contracts extends Model
{
    protected $table = 'contracts';
    protected $fillable = ['client_id', 'admin_id', 'insurance_type_id', 'branch_id', 'status', 'tariff', 'sum_insured'];
    protected $with = ['client', 'admin', 'insurance', 'branch'];

    public function client()
    {
        return $this->hasOne(User::class, 'id', 'client_id');
    }

    public function admin()
    {
        return $this->hasOne(User::class, 'id', 'admin_id');
    }

    public function insurance()
    {
        return $this->hasOne(InsuranceType::class, 'id', 'insurance_type_id');
    }

    public function branch()
    {
        return $this->hasOne(Branch::class, 'id', 'branch_id');
    }
}
