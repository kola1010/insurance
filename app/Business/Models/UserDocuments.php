<?php

namespace App\Business\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;

class UserDocuments extends Model
{
    protected $table = 'user_documents';
    protected $fillable = ['user_id', 'pass_series', 'pass_code', 'pass_address'];
    protected $with = ['user'];

    public function user()
    {
        return $this->hasOne(User::class, 'id', 'user_id');
    }
}
