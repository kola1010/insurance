<?php

namespace App\Business\Models;

use Illuminate\Database\Eloquent\Model;

class Branch extends Model
{
    protected $table = 'branches';
    protected $fillable = ['phone', 'name', 'address'];
    protected $with = ['workers'];

    public function workers()
    {
        return $this->hasMany(BranchWorkers::class, 'branch_id', 'id');
    }
}
