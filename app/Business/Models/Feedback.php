<?php

namespace App\Business\Models;

use Illuminate\Database\Eloquent\Model;

class Feedback extends Model
{
    protected $table = 'feedback';
    protected $fillable = ['name', 'phone', 'email', 'type'];
}
