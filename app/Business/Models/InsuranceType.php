<?php

namespace App\Business\Models;

use Illuminate\Database\Eloquent\Model;

class InsuranceType extends Model
{
    protected $table = 'insurance_types';
    protected $fillable = ['type'];
}
