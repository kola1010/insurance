<?php

namespace App\Business\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;

class BranchWorkers extends Model
{
    protected $table = 'branch_workers';
    protected $fillable = ['worker_id', 'branch_id'];

    public function worker()
    {
        return $this->hasOne(User::class, 'id', 'worker_id');
    }

    public function branch()
    {
        return $this->hasOne(Branch::class, 'id', 'branch_id');
    }
}
