<?php

namespace App\Http\Middleware;

use Closure;

class AdminMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (auth() && auth()->user()->is_admin)
        {
            return $next($request);
        }
        return redirect(route('front.homepage'))->with('error', 'Вы не являетесь администратором.');
    }
}
