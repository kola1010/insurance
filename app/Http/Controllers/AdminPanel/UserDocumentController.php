<?php

namespace App\Http\Controllers\AdminPanel;

use App\Business\Models\UserDocuments;
use App\Http\Controllers\Controller;
use App\Http\Requests\UpdateUserDocumentsRequest;
use Illuminate\Http\Request;

class UserDocumentController extends Controller
{
    public function show($userId)
    {
        $userDocument = UserDocuments::where('user_id', $userId)->first();
        return view('admin-layouts.user-documents.show', compact('userDocument'));
    }

    public function update(UpdateUserDocumentsRequest $request, UserDocuments $userDocument)
    {
        $userDocument->fill($request->all());
        $userDocument->save();
        return redirect()->back()->with('success', 'Данные успешно сохранены.');
    }
}
