<?php

namespace App\Http\Controllers\AdminPanel;

use App\Business\Models\InsuranceType;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class InsuranceTypeController extends Controller
{
    public function index(Request $request)
    {
        $insurances = InsuranceType::orderByDesc('created_at')->paginate(20);
        return view('admin-layouts.insurance-type.index', compact('insurances'));
    }

    public function show(InsuranceType $insuranceType)
    {
        return view('admin-layouts.insurance-type.show', compact('insuranceType'));
    }

    public function create(Request $request)
    {
        return view('admin-layouts.insurance-type.create');
    }

    public function update(Request $request, InsuranceType $insuranceType)
    {
        $insuranceType->fill($request->all());
        $insuranceType->save();
        return redirect()->back()->with('success', 'Успешно обновлено');
    }

    public function store(Request $request, InsuranceType $insuranceType)
    {
        $insuranceType = $insuranceType->create($request->except('_token'));
        return redirect()->route('admin.insurance-type.show', $insuranceType);
    }

    public function destroy(InsuranceType $insuranceType)
    {
        $insuranceType->delete();
        return redirect()->route('admin.insurance-type.index')->with('success', 'Тип успешно удален');
    }
}
