<?php

namespace App\Http\Controllers\AdminPanel;

use App\Business\Models\Feedback;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class FeedbackController extends Controller
{
    public function index()
    {
        $feedbacks = Feedback::orderByDesc('created_at')->paginate(20);
        return view('admin-layouts.feedback.index', compact('feedbacks'));
    }
}
