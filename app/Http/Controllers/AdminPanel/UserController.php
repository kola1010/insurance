<?php

namespace App\Http\Controllers\AdminPanel;

use App\Business\Models\BranchWorkers;
use App\Business\Models\Contracts;
use App\Business\Models\UserDocuments;
use App\Http\Controllers\Controller;
use App\Http\Requests\CreateUserRequest;
use App\Http\Requests\UpdateUserRequest;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    public function index(User $users)
    {
        $users = $users->paginate(20);
        return view('admin-layouts.users.index', compact('users'));
    }

    public function create()
    {
        return view('admin-layouts.users.create');
    }

    public function store(CreateUserRequest $request, User $user, UserDocuments $userDocuments)
    {
        $user = $user->create($request->except(['_token']));
        $userDocuments->create(['user_id' => $user->id]);
        return redirect()->route('admin.users.show', $user);
    }

    public function show(User $user)
    {
        return view('admin-layouts.users.show', compact('user'));
    }

    public function update(UpdateUserRequest $request, User $user)
    {
        $data = $request->all();
        if ($request->new_password) {
            $data['password'] = Hash::make($request->new_password);
        }
        $user->fill($data);
        $user->save();
        return redirect()->back()->with('success', 'Данные успешно сохранены.');
    }

    public function destroy(User $user, UserDocuments $userDocuments, Contracts $contracts, BranchWorkers $branchWorkers)
    {
        $userDocuments->where('user_id', $user->id)->delete();
        $contracts->where('client_id', $user->id)->delete();
        if ($user->is_admin) {
            $branchWorkers->where('worker_id', $user->id)->delete();
        }
        $user->delete();
        return redirect()->route('admin.users.index')->with('success', 'Пользователь успешно удален');
    }
}
