<?php

namespace App\Http\Controllers\AdminPanel;

use App\Business\Models\Branch;
use App\Http\Controllers\Controller;
use App\Http\Requests\BranchRequest;
use Illuminate\Http\Request;

class BranchController extends Controller
{
    public function index(Branch $branch)
    {
        $branches = $branch->paginate(20);
        return view('admin-layouts.branch.index', compact('branches'));
    }

    public function show(Branch $branch)
    {
        return view('admin-layouts.branch.show', compact('branch'));
    }

    public function create()
    {
        return view('admin-layouts.branch.create');
    }

    public function store(BranchRequest $request, Branch $branch)
    {
        $branch = $branch->create($request->except('_token'));
        return redirect()->route('admin.branch.show', $branch);
    }

    public function update(BranchRequest $request, Branch $branch)
    {
        $branch->fill($request->all());
        $branch->save();
        return redirect()->back()->with('success', 'Филиал успешно сохранен');
    }
}
