<?php

namespace App\Http\Controllers\AdminPanel;

use App\Business\Models\Branch;
use App\Business\Models\Contracts;
use App\Business\Models\InsuranceType;
use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ContractController extends Controller
{
    public function index()
    {
        $contracts = Contracts::orderByDesc('created_at')->paginate(20);

        return view('admin-layouts.contracts.index', compact('contracts'));
    }

    public function show(Contracts $contract)
    {
        $clients = User::where('is_admin', 0)->get();
        $workers = User::where('is_admin', 1)->get();
        $insuranceTypes = InsuranceType::get();
        $branches = Branch::get();

        return view('admin-layouts.contracts.show', compact('contract', 'clients','insuranceTypes', 'branches','workers'));
    }

    public function create()
    {
        $clients = User::where('is_admin', 0)->get();
        $workers = User::where('is_admin', 1)->get();
        $insuranceTypes = InsuranceType::get();
        $branches = Branch::get();
        return view('admin-layouts.contracts.create', compact('clients','insuranceTypes', 'branches','workers'));
    }

    public function update(Request $request, Contracts $contract)
    {
        $contract->fill($request->all());
        $contract->save();
        return redirect()->back()->with('success', 'Успешно обновлено');
    }

    public function store(Request $request)
    {
        $contract = new Contracts();
        $contract = $contract->create($request->except('_token'));
        return redirect()->route('admin.contracts.show', $contract);
    }

    public function destroy(Contracts $contract)
    {
        $contract->delete();
        return redirect()->route('admin.contracts.index')->with('success', 'Контракт успешно удален.');
    }
}
