<?php

namespace App\Http\Controllers\AdminPanel;

use App\Business\Models\Branch;
use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Http\Request;
use App\Business\Models\BranchWorkers;

class BranchWorkersController extends Controller
{
    public function show($branchId)
    {
        $branch = Branch::findOrFail($branchId);
        return view('admin-layouts.branch-workers.show', compact('branch'));
    }

    public function edit(BranchWorkers $branchWorker)
    {
        return view('admin-layouts.branch-workers.edit', compact('branchWorker'));
    }

    public function create(Request $request)
    {
        $branch = Branch::findOrFail($request->get('branch_id'));
        $workers = User::where('is_admin', 1)->get();
        return view('admin-layouts.branch-workers.create', compact('branch', 'workers'));
    }

    public function store(Request $request, BranchWorkers $branchWorkers)
    {
        if (BranchWorkers::where(['branch_id' => $request->post('branch_id'), 'worker_id' => $request->post('worker_id')])->first()) {
            return redirect()->back()->with('error', 'Данный пользователя уже является сотрудником данного филиала!');
        }
        $branchWorkers->create($request->except('_token'));
        return redirect()->route('admin.branch-workers.show', $request->post('branch_id'))->with('success', 'Работник успешно создан');
    }

    public function update(Request $request, BranchWorkers $branchWorkers)
    {
        $branchWorkers->fill($request->except('_token'))->save();
        return redirect()->back()->with('success', 'Работник успешно обновлен');
    }

    public function destroy(Request $request, $branchWorkerId)
    {
        BranchWorkers::find($branchWorkerId)->delete();
        return redirect()->route('admin.branch-workers.show', $request->branch_id);
    }
}
