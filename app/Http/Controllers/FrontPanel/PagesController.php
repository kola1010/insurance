<?php

namespace App\Http\Controllers\FrontPanel;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class PagesController extends Controller
{
    public function travelPage()
    {
        return view('front-layouts.pages.travel');
    }

    public function estatePage()
    {
        return view('front-layouts.pages.estate');
    }

    public function autoPage()
    {
        return view('front-layouts.pages.auto');
    }

    public function contactsPage()
    {
        return view('front-layouts.pages.contacts');
    }
}
