<?php

namespace App\Http\Controllers\FrontPanel;

use App\Business\Models\Feedback;
use App\Http\Controllers\Controller;
use App\Http\Requests\FeedbackRequest;
use Illuminate\Http\Request;

class FeedbackController extends Controller
{
    public function store(FeedbackRequest $request, Feedback $feedback)
    {
        $feedback->fill($request->all());
        $feedback->save();
        return redirect()->back()->with('success', 'Ваша заявка будет обработана в течении часа.');
    }

    public function show()
    {
        return view('front-layouts.pages.feedback');
    }
}
