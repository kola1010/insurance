<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::group(['as' => 'front.', 'namespace' => 'FrontPanel'], function() {
    Route::post('feedbackStore', 'FeedbackController@store')->name('feedbackSubmit');
    Route::get('feedback', 'FeedbackController@show')->name('feedback');

    Route::group(['as' => 'pages.', 'prefix' => 'page'], function() {
        Route::get('travel', 'PagesController@travelPage')->name('travel');
        Route::get('estate', 'PagesController@estatePage')->name('estate');
        Route::get('auto', 'PagesController@autoPage')->name('auto');
    });

    Route::get('contacts', 'PagesController@contactsPage')->name('contacts');
    Route::get('about-us', 'PagesController@aboutUsPage')->name('aboutUs');

    Route::get('/', 'HomeController@index')->name('homepage');
});

Route::group(['as' => 'admin.', 'namespace' => 'AdminPanel', 'prefix' => 'admin-panel'], function() {
    Route::group(['middleware' => 'Admin'], function() {
        Route::resource('users', 'UserController');
        Route::resource('branch', 'BranchController');
        Route::resource('branch-workers', 'BranchWorkersController');
        Route::resource('feedback', 'FeedbackController');
        Route::resource('insurance-type', 'InsuranceTypeController');
        Route::resource('user-documents', 'UserDocumentController');
        Route::resource('contracts', 'ContractController');
    });
});